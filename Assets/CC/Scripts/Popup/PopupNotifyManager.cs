﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class PopupNotifyManager : MonoBehaviour {

	
	[SerializeField]
	RectTransform rectText;
	[SerializeField]
	Text txt;

	static PopupNotifyManager instance;
	public static PopupNotifyManager Instance{
		get {
			return instance;
		}
	}
	void Awake()
	{
		instance = this;
		gameObject.SetActive (false);

		//Show (10, "Hồn lỡ sa vào đôi mắt em....!");
	}
	const float PartWidth = 100;

	public void Show(int _loop, string _msg)
	{
		txt.text = _msg;
		float pos = (_msg.Length / 5 + 1) * PartWidth;

		rectText.anchoredPosition = new Vector2 (pos, 0);
		rectText.pivot = new Vector2 (1, .5f);

		gameObject.SetActive (true);


		rectText.DOAnchorPos (new Vector2(-1900,0), 7 + _msg.Length/4 ).SetLoops (_loop).SetEase (Ease.Linear).OnComplete (() => {
            gameObject.SetActive(false);
		});
	}
}
