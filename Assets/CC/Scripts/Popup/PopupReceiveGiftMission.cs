﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupReceiveGiftMission : MonoBehaviour {
    [SerializeField]
    GameObject PanelReceiveGiftMission;
    [SerializeField]
    Image BgLightEffect, ImgCircleRaw, ImgGold;
    [SerializeField]
    Text TxtCircle, TxtGold;
    Animator BtnAnim;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Show()
    {
        return;
        //PanelReceiveGiftMission.SetActive(true);
        //if(!BtnAnim) BtnAnim = BgLightEffect.gameObject.GetComponent<Animator>();
        //BtnAnim.SetBool("play", true);
        //iTween.MoveTo(ImgGold.gameObject, iTween.Hash("y", 90f, "oncomplete", "myClbk", "oncompletetarget", gameObject, "time",3.0f));
    }
    public void myClbk()
    {
        Debug.Log("PopupReceiveGiftMission==== myClbk");
        Hide();
    }
    public void Hide()
    {
        if (!BtnAnim) BtnAnim = BgLightEffect.gameObject.GetComponent<Animator>();
        BtnAnim.SetBool("play", false);
        PanelReceiveGiftMission.SetActive(false);
    }
}
