﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseCallBack;
using SimpleJSON;

public class PanelLichSuNap : MonoBehaviour
{
    [SerializeField]
    GameObject Me, ContentApp, ContentCarrd, ScrollView_App, ScrollView_Card, Title_App, Title_Card, ItemApp, ItemCard, BtnApp, BtnCard;
    Button Card, App;

    public void Init()
    {
        Card = BtnCard.GetComponent<Button>();
        App = BtnApp.GetComponent<Button>();

        BtnApp.transform.GetChild(0).gameObject.SetActive(true);
        BtnCard.transform.GetChild(0).gameObject.SetActive(false);
        API.Instance.RequestCardHisData(RspHisCard);
        Dictionary<string, DataNap> dictInApp = GameHelper.dictHisBuyInApp;
        if (dictInApp.Count > 0)
        {
            foreach (var item in dictInApp)
            {
                DataNap dt = item.Value;
                TaoItemApp(dt.Gia, dt.Token, dt.store, dt.RspPaymentInApp);
            }
        }
        else
        {
            Debug.LogWarning("k co du lieu nap loi app");
        }


        ScrollView_Card.SetActive(false);
        Title_Card.SetActive(false);

        ScrollView_App.SetActive(true);
        Title_App.SetActive(true);


        if (!GameHelper.EnablePayment)
        {
            BtnCard.SetActive(false);
        }
        else
        {
            BtnCard.SetActive(true);
        }
    }

    public void TaoItemApp(string Gia, string Token, string Store, onCallBackString rspPaymentInApp)
    {
        GameObject it = Instantiate(ItemApp, ContentApp.transform) as GameObject;

        ItemHisInApp itcs = it.GetComponent<ItemHisInApp>();
        itcs.Init(Gia, Token, Store, rspPaymentInApp);
    }


    public void TaoItemCard(string ThoiGian, string Gia, string loai, string trangthai, string pin, string seri)
    {
        GameObject it = Instantiate(ItemCard, ContentCarrd.transform) as GameObject;

        ItemHisCard itcs = it.GetComponent<ItemHisCard>();
        itcs.Init(ThoiGian, Gia, loai, trangthai, pin, seri);
    }


    public void ClearContentApp()
    {
        int cn = ContentApp.transform.GetChildCount();
        //Debug.LogWarning("cn === " + cn);
        for (int i = 0; i < cn; i++)
        {
            GameObject it = ContentApp.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }
    }

    public void ClearContentCard()
    {
        int cn = ContentCarrd.transform.GetChildCount();
        //Debug.LogWarning("cn === " + cn);
        for (int i = 0; i < cn; i++)
        {
            GameObject it = ContentCarrd.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }
    }

    public void BtnAppClick()
    {
        BtnApp.transform.GetChild(0).gameObject.SetActive(true);
        BtnCard.transform.GetChild(0).gameObject.SetActive(false);


        ScrollView_Card.SetActive(false);
        Title_Card.SetActive(false);

        ScrollView_App.SetActive(true);
        Title_App.SetActive(true);
        //ClearContentApp();
    }

    public void BtnCardClick()
    {
        BtnApp.transform.GetChild(0).gameObject.SetActive(false);
        BtnCard.transform.GetChild(0).gameObject.SetActive(true);


        ScrollView_Card.SetActive(true);
        Title_Card.SetActive(true);

        ScrollView_App.SetActive(false);
        Title_App.SetActive(false);

        

    }




    void RspHisCard(string _json)
    {
        if (_json != "->>>>Data empty")
        {
            JSONNode node = JSONNode.Parse(_json);
            Debug.LogWarning(" RspHisCard==== " + _json);
            // Debug.LogWarning(" RspHisCard====DataCount " + node["data"].Count);
            int count = node["data"].Count;
            for (int i = 0; i < count; i++)
            {
                string thoigian = node["data"][i]["created_date"];
                string gia = node["data"][i]["info"]["amount"];
                string loai = node["data"][i]["from"];
                string st = node["data"][i]["status"];
                string trangthai = "";
                if (st == "1")
                {
                    trangthai = "Giao dịch thành công";
                }
                else if (st == "-1")
                {
                    trangthai = "Mã thẻ không đúng hoặc thẻ đã được sử dụng";
                }

                string pin = node["data"][i]["info"]["code"];
                string seri = node["data"][i]["info"]["serial"];


                TaoItemCard(thoigian, gia, loai, trangthai, pin, seri);
            }
        }
        
    }


    public void OpenMe()
    {
        Init();
        Me.SetActive(true);
    }

    public void CloseMe()
    {
        ClearContentApp();
        ClearContentCard();
        Me.SetActive(false);
    }
}
