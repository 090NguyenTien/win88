﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using SimpleJSON;

public class PopupFogetPasswordManager : MonoBehaviour {

	[SerializeField]
	GameObject panel;
	[SerializeField]
	InputField inputEmail;
	[SerializeField]
	Button btnCancel, btnOk;
	[SerializeField]
	PopupAlertManager Alert;
	[SerializeField]
	Text txtError;


	bool IsInited = false;

	public void ShowPanel(){
		panel.SetActive (true);
		inputEmail.text = "";
		ShowError ();
		HideALert ();
	}
	public void ShowAlert(string _msg){
		Alert.Show (_msg, Alert.Hide);
	}
	public void HidePanel(){
		panel.SetActive (false);
	}
	public void HideALert(){
		Alert.Hide ();
	}

	public void Init(){
		if (IsInited)
			return;

		btnCancel.onClick.AddListener (BtnCancelOnClick);
		btnOk.onClick.AddListener (BtnOkOnClick);

		Alert.Init ();

		IsInited = true;
	}

	void BtnCancelOnClick ()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		HidePanel ();
		HideALert ();
	}

	void BtnOkOnClick ()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		ShowError ();

		string _mail = inputEmail.text;
		if (string.IsNullOrEmpty (_mail))
			ShowError ("Vui lòng nhập địa chỉ email.");
		else if (!_mail.Contains ("@") || !_mail.Contains ("."))
			ShowError ("Địa chỉ email không hợp lệ");
		else {

			HidePanel ();
			API.Instance.RequestFogetPassword (_mail, RspFogetPass);

		}
	}

	void RspFogetPass (string _json)
	{
		JSONNode node = JSONNode.Parse (_json);

//		ShowError (node ["message"]);
		ShowAlert (node ["message"]);
	}

	void ShowError(string _msg = ""){
		txtError.text = _msg;
	}
}
