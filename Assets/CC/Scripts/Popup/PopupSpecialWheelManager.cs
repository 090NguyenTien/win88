﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;
using SimpleJSON;
using BaseCallBack;
using Sfs2X.Entities.Data;
using UnityEngine.Purchasing;
using System;

public class PopupSpecialWheelManager : MonoBehaviour
{
	[SerializeField]
	GameObject panel;
	[SerializeField]
	Transform trsfLuckyWheel;
	[SerializeField]
	Button btnStart, btnBack, btnBuyTurn, btnOne,btnFive;
	[SerializeField]
	Text txtChip, txtRule, txtTimeRemain, txtTimeRemainVirtual;
	[SerializeField]
	Image imgFree;
	[SerializeField]
	Sprite sprLightTrue, sprLightFalse;
	[SerializeField]
	List<GameObject> lstAnimChip;
	[SerializeField]
	Image imgBtnPlay, imgIconPlay;
	[SerializeField]
	GameObject CountDown;
    [SerializeField]
    PopupShopManager poupupShopManager;

    [SerializeField]
    private Animator animLighting = null;

	int Turn;
    [SerializeField]
    private Text txtTurnCount = null;

	[HideInInspector]
	public bool IsRolling = false;

    [SerializeField]
    Text txtChipRequired, txtChipUser, txtGemUser, txtLuckyPlus;

    bool IsInited = false;

    public void showPopupSpecWhell()
    {
        Debug.Log("Nhan nay!!!!");
        Init();
        //UM_InAppPurchaseManager.OnPurchaseFlowFinishedAction += OnPuchased;
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.CallServiceGetProductInAppId();
    }

    void OnPuchased(string productId, string purchaseToken, bool isSuccess)
    {
        
        if (isSuccess)
        {

            //if (_result.Google_PurchaseInfo != null)
            //    Debug.Log(_result.Google_PurchaseInfo);
            //else if (_result.IOS_PurchaseInfo != null)
            //    Debug.Log(_result.IOS_PurchaseInfo);

#if UNITY_ANDROID
            //JSONNode node = JSONNode.Parse(_result.Google_PurchaseInfo.originalJson);

            string store = "googleplay";
            string productID = productId;//node["productId"].Value;
            string token = purchaseToken;//node["purchaseToken"].Value;

            API.Instance.RequestPaymentWheelInAppAndroid(store, productID, token, RspPaymentWheelInApp);

#elif UNITY_IOS

//			JSONNode node = JSONNode.Parse(_result.IOS_PurchaseInfo.Receipt);
//
			string store = "appstore";
			string receipt = _result.IOS_PurchaseInfo.Receipt;

//			s += "TOKEN: " + token
//			+ "\n packageName " + productID + "\n";

//			s+= "AppUname: " + _result.IOS_PurchaseInfo.ApplicationUsername
//				+"\nProductID: " + _result.IOS_PurchaseInfo.ProductIdentifier
//				+"\nReceipt: " + _result.IOS_PurchaseInfo.Receipt
//				+"\nTransactionId: " + _result.IOS_PurchaseInfo.TransactionIdentifier
//				;
            API.Instance.RequestPaymentInAppIOS(store, receipt, RspPaymentWheelInApp);
#endif

        }
        else
        {
            AlertController.api.showAlert("Giao dịch không thành công.");
        }
    }

    void RspPaymentWheelInApp(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" RspPaymentInApp " + _json);
        if (node["status"].AsInt == 1)
        {
            int moreWheel = int.Parse(node["wheel"].Value);
            
            Turn = int.Parse(node["current_wheel"].Value);
            txtTurnCount.text = Turn.ToString();
            MyInfo.SPEC_WHEEL = Turn;
            AlertController.api.showAlert(node["msg"].Value);
            closeBuyTurn();
        }
        else if (node["status"].AsInt == 0)
        {
            AlertController.api.showAlert(node["sms"]);
        }

    }










    public void Init()
	{
        if (MyInfo.IS_APK_PURE)
        {
            btnOne.gameObject.SetActive(false);
            btnFive.gameObject.SetActive(false);
        }
        gameObject.SetActive(true);
		if (IsInited)
			return;
        Debug.Log (" ======= INIT LUCKY WHEEL");
        btnStart.onClick.RemoveAllListeners();
        //btnStart.onClick.AddListener (StartWheelOnClick);
        btnBuyTurn.onClick.RemoveAllListeners();
        btnBuyTurn.onClick.AddListener(openBuyTurn);
        txtTurnCount.text = Turn.ToString();
        MyInfo.SPEC_WHEEL = Turn;
        btnBack.onClick.RemoveAllListeners();
        btnBack.onClick.AddListener(BtnBackOnClick);
        
        GamePacket gamePacket = new GamePacket(CommandKey.GET_SPEC_WHEEL);
#if UNITY_ANDROID
        gamePacket.Put("os", "android");
#elif UNITY_IPHONE
        gamePacket.Put("os", "ios");
        gamePacket.Put("bundle_id", Application.identifier);
#endif


        SFS.Instance.SendZoneRequest(gamePacket);


        IsInited = true;
	}

    
    public void resGetSpecWheel(GamePacket data)
    {
        Turn = data.GetInt("w");
        txtTurnCount.text = Turn.ToString();
        MyInfo.SPEC_WHEEL = Turn;
        if (data.ContainKey("wcr"))
        {
            MyInfo.SPEC_WHEEL_CHIP_REQUIRED = data.GetLong("wcr");
            txtChipRequired.text = Utilities.GetStringMoneyByLongBigSmall(MyInfo.SPEC_WHEEL_CHIP_REQUIRED);
        }
        if (data.ContainKey("lp"))
        {
            MyInfo.LUCKY_SPEC_WHEEL = data.GetInt("lp");
            txtLuckyPlus.text = "+" + MyInfo.LUCKY_SPEC_WHEEL.ToString() + "% May Mắn";
        }
        productOne = data.param.GetSFSObject("wp1").GetUtfString("pid");
        productFive = data.param.GetSFSObject("wp5").GetUtfString("pid");
        
        Debug.Log("resGetSpecWheel "+ productOne + " - " + productFive);
    }






	public void BtnBackOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);
        //UM_InAppPurchaseManager.OnPurchaseFlowFinishedAction -= OnPuchased;
        gameObject.SetActive(false);
	}

    [SerializeField]
    private GameObject buyTurnPanel = null;
    public void closeBuyTurn()//call editor
    {
        buyTurnPanel.gameObject.SetActive(false);
    }

    public void openBuyTurn()
    {
        buyTurnPanel.gameObject.SetActive(true);
    }

	public void Hide()
	{
		panel.SetActive (false);
	}

	public void StartWheelOnClick(int type)//type 1 : vong quay - 2 : chip
	{
		if (IsRolling)
			return;
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);
        if (type == 1)
        {
            if (MyInfo.SPEC_WHEEL <= 0)
            {
                AlertController.api.showAlert("Bạn đã hết lượt! bạn có thể mua thêm tại cửa hàng", openBuyTurn);
                return;
            }
        }else
        {
            if (MyInfo.CHIP < MyInfo.SPEC_WHEEL_CHIP_REQUIRED)
            {
                AlertController.api.showAlert("Bạn đã hết chip! bạn có thể mua thêm tại cửa hàng", openShop);
                return;
            }
        }
        IsRolling = true;
        GamePacket gamePacket = new GamePacket(CommandKey.START_ROLL_SPEC_WHEEL);
        gamePacket.Put("wt", type);
        SFS.Instance.SendZoneRequest(gamePacket);




    }

    private void openShop()
    {
        poupupShopManager.Show();
    }

    private string productOne = "", productFive = "";
    public void buyPackOne()//call editor
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        //UM_InAppPurchaseManager.Instance.Purchase(productOne);
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.OnPurchaseClicked(productOne, OnPuchased);
    }

    public void buydPackFive()//call editor
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        //UM_InAppPurchaseManager.Instance.Purchase(productFive);
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.OnPurchaseClicked(productFive, OnPuchased);
    }










    public void onStartWheelResponse(GamePacket dataWheel)
    {
        Debug.Log(dataWheel.GetInt("rt").ToString());//rt = 1 chip hoac gem // rt=2 la card
        if (dataWheel.ContainKey("wcr"))
        {
            MyInfo.SPEC_WHEEL_CHIP_REQUIRED = dataWheel.GetLong("wcr");
        }
        if (dataWheel.ContainKey("chip"))
        {
            MyInfo.CHIP = dataWheel.GetLong("chip");
            txtChipUser.text = Utilities.GetStringMoneyByLongBigSmall(MyInfo.CHIP);
        }
        if (dataWheel.ContainKey("gem"))
        {
            MyInfo.GEM = dataWheel.GetLong("gem");
            txtGemUser.text = MyInfo.GEM.ToString();
        }
        if (dataWheel.ContainKey("we"))
        {
            IsRolling = false;
            ShowError("Bạn đã hết lượt! Bạn có thể mua thêm tại cửa hàng", openBuyTurn);
        }
        else
        {
            int index = dataWheel.GetInt("wi");
            wheelResult(index, dataWheel);
            Turn = dataWheel.GetInt("w");
            txtTurnCount.text = Turn.ToString();
            MyInfo.SPEC_WHEEL = Turn;
        }

        
    }

    private void wheelResult(int index, GamePacket dataWheel)
    {
        StartLuckyWheel(index, () =>
        {
            if (dataWheel.ContainKey("lp"))
            {
                MyInfo.LUCKY_SPEC_WHEEL = dataWheel.GetInt("lp");
                txtLuckyPlus.text = "+" + MyInfo.LUCKY_SPEC_WHEEL.ToString() + "% May Mắn";
            }
            txtChipRequired.text = Utilities.GetStringMoneyByLongBigSmall(MyInfo.SPEC_WHEEL_CHIP_REQUIRED);//update mức tiền kế tiếp
            GamePacket dataWheelw = dataWheel;
            int giftType = dataWheel.GetInt("rt");
            string alertResult = "";
            //int reward = dataWheel.GetInt("crw");//chip reward
            int reward = dataWheel.GetInt("grw");//gem reward
            if (giftType == 1)
            {
                alertResult = reward.ToString() + " kim cương"; //Utilities.GetStringMoneyByLong(reward) + " chip";
                //alertResult = Utilities.GetStringMoneyByLong(reward) + " chip";
            }
            else
            {
                alertResult = "1 thẻ cào " + dataWheel.GetString("tc") + " " + dataWheel.GetInt("ca").ToString() + " \n Bạn check hộp thư để nhận phần thưởng nhé";
                PopupInboxManager.Instance.addNotification();
            }
            IsRolling = false;
            ShowError("Chúc mừng bạn đã nhận được " + alertResult, () =>
            {

                if (reward > 0)
                {
                    MyInfo.GEM += reward;
                    //AnimHomeController.api.playGold(reward, 20, btnStart.transform.position, HomeViewV2.api.getGoldTranform());
                }
                
            });
        });
    }


    public void TestAnim()
    {
        int rewardChip = 1000000;
        AnimHomeController.api.playGold(rewardChip, 20, btnStart.transform.position, HomeViewV2.api.getGoldTranform());
    }



    
    private float TimeWheel = 7;
	void StartLuckyWheel(int _index, onCallBack _callBack = null)
	{
		Vector3 result = GetRotationZ (_index);
       // Debug.LogError("index chip nek:   " + _index + "    result   " + result);

        animLighting.SetTrigger("ShowWheeling");

        trsfLuckyWheel.DOKill(false);
        trsfLuckyWheel.DORotate (result, UnityEngine.Random.Range(TimeWheel - 2, TimeWheel + 4), RotateMode.FastBeyond360).SetEase (easeType).OnComplete (() => 
        {
			if (_callBack != null)
			    _callBack();

            animLighting.SetTrigger("ShowWheelComplete");

		});
	}

	void ShowError(string _msg, onCallBack _callBack)
	{
        AlertController.api.showAlert(_msg);
        if (_callBack != null)
        {
            _callBack();
        }
        Debug.Log(_msg);
	}

    int numPart = 18;//so phan chia tren wheel
    const int angle = 20;//1 phan chiem 45 do
    const int Loop = 20;
    private Ease easeType = Ease.InOutQuint;

    Vector3 GetRotationZ(int _index)
	{
        Debug.Log(_index);
        int centerAngle = 360 - _index * angle;
        int deltaAngle = UnityEngine.Random.Range(-angle / 3 , angle / 3 );
        int rotation = centerAngle + deltaAngle;
        float result = (rotation - (UnityEngine.Random.Range(Loop, Loop + 6) * 360));
        return new Vector3 (0, 0, result);
	}
}
