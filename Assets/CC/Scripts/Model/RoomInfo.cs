﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomInfo
{

    public int roomId;
    public long bettingMoney;
    public long require;
    public int playerNumber;
    public bool isPass;
    public string host;
    public bool isPlaying;
    public RoomInfo(string strRoomData)
    {
        string[] roomInfo = strRoomData.Split('#');        
        roomId = int.Parse(roomInfo[0]);
        bettingMoney = long.Parse(roomInfo[1]);
        require = long.Parse(roomInfo[2]);
        playerNumber = int.Parse(roomInfo[3]);
        isPass = roomInfo[4].Equals("1");
        host = roomInfo[5];
        isPlaying = roomInfo[6].Equals("1");
    }
}
