﻿using UnityEngine;
using System.Collections;

public class DontDestroyManager : MonoBehaviour {

	public static DontDestroyManager instance;

	void Awake()
	{
//		if (instance) {
//			Destroy (gameObject);
//			return;
//		}
		instance = this;
		DontDestroyOnLoad (gameObject);
	}
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            AlertController.api.showAlert("Bạn có muốn thoát game?", Application.Quit, true);
        }
    }
}
