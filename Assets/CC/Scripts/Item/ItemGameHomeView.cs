﻿using UnityEngine;
using System.Collections;

public class ItemGameHomeView : MonoBehaviour {

	[SerializeField]
	int gameID;


	void OnMouseDown(){
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		GameObject.FindGameObjectWithTag ("GameController").SendMessage ("RequestJoinGameLobbyRoom", gameID);
	}
}
