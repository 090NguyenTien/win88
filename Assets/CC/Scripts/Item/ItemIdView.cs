﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;

public class ItemIdView : MonoBehaviour {

	Button btn;
	Text txtMsg;

	onCallBackInt onClick;
    
	int index;
    [SerializeField]
    ClockItemBoom clockitemboom;

    public void Init(int _index, onCallBackInt _onClick)
	{
		index = _index;
		onClick = _onClick;

		btn = GetComponent<Button> ();
        btn.onClick.RemoveListener(ItemOnClick);
        btn.onClick.AddListener (ItemOnClick);
	}

	public void InitText(string _text){

		txtMsg = transform.GetChild (0).GetComponent<Text> ();
		txtMsg.text = _text;
	}

	void ItemOnClick()
	{
		onClick (index);
        clockitemboom.StartAnim();

    }
}
