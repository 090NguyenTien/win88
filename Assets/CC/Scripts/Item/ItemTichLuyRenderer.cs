﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class ItemTichLuyRenderer : MonoBehaviour {

    [SerializeField]
    Text txtTarget, txtChipReceive, txtVongQuay, txtDaNhan;
    [SerializeField]
    Button btnReceive;
    GameObject panelReceiveGift;
    long target, chipReceive, curTotal;
    int vongquay;
    JSONNode receiveIndex;
    int index;
    public void Init(long curTotal, long target, long chipReceive, int vongquay, int index,JSONNode receiveIndex, GameObject panelReceiveGift)
    {
        this.panelReceiveGift = panelReceiveGift;
        this.curTotal = curTotal;
        this.index = index;
        this.target = target;
        this.chipReceive = chipReceive;
        this.vongquay = vongquay;
        this.receiveIndex = receiveIndex;

        //txtTarget.text = target.ToString();
        CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
        txtTarget.text = double.Parse(target.ToString()).ToString("#,###", cul.NumberFormat) + "";

        //txtChipReceive.text = chipReceive.ToString();
        txtChipReceive.text = double.Parse(chipReceive.ToString()).ToString("#,###", cul.NumberFormat);

        txtVongQuay.text = vongquay.ToString();
        btnReceive.gameObject.SetActive(false);
        txtDaNhan.gameObject.SetActive(false);
        gameObject.SetActive(true);
        btnReceive.onClick.AddListener(ReceiveGift);


        StatusReceive();       
    }
    private void StatusReceive()
    {
        bool isAllowReceive = false;
        if (curTotal >= target)
        {
            isAllowReceive = true;
            btnReceive.gameObject.SetActive(true);
            txtDaNhan.gameObject.SetActive(false);
        }
        if (receiveIndex != null)
        {
            for (int i = 0; i < receiveIndex.Count; i++)
            {
                if (index == int.Parse(receiveIndex[i]))
                {
                    btnReceive.gameObject.SetActive(false);
                    if(isAllowReceive) txtDaNhan.gameObject.SetActive(true);
                    break;
                }
            }
        }
        
    }

    private void ReceiveGift()
    {
        btnReceive.gameObject.SetActive(false);
        txtDaNhan.gameObject.SetActive(true);
        API.Instance.ReceiveNapTichLuy(index, ResReceive);
    }
    private void ResReceive(string s)
    {
        Debug.Log("ResReceiveNapTichLuy: " + s);
        JSONNode node = JSONNode.Parse(s);
        //GameHelper.IsUpgradeVip = node["msg_upgrade_vip"].Value;
        int status = int.Parse(node["status"].Value);
        if (status == 1)
        {
            JSONNode bonus = node["bonus"];
            JSONNode final = node["final"];
            panelReceiveGift.SetActive(true);
            BaseGiftReceive giftReceive = panelReceiveGift.GetComponent<BaseGiftReceive>();
            giftReceive.Init(long.Parse(bonus["chip"]), int.Parse(bonus["wheel"]),gameObject, true);
            MyInfo.CHIP = long.Parse(final["chip"]);
            MyInfo.SPEC_WHEEL = int.Parse(final["wheel"]);           
        }
        else if (status == -1) AlertController.api.showAlert("Chưa có mốc tích lũy nào", onCallBackPopupAlert);
        else if (status == -2) AlertController.api.showAlert("Đã nhận mốc này rồi", onCallBackPopupAlert);
        else if (status == -3) AlertController.api.showAlert("Mốc tích lũy chưa được thiết lập", onCallBackPopupAlert);
        else if (status == -4) AlertController.api.showAlert("Chưa đủ điều kiện nhận mốc", onCallBackPopupAlert);
        else AlertController.api.showAlert("Không Thể Nhận Thưởng", onCallBackPopupAlert);
    }
    private void onCallBackPopupAlert()
    {
        
    }
    public void OnDestroy()
    {
        btnReceive.onClick.RemoveListener(ReceiveGift);
    }
}
