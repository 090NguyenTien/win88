﻿using BaseCallBack;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.Security;


namespace UnityPurchasinghelp
{
public class UnityPurchasingHelper : MonoBehaviour, IStoreListener
    {
    private onCallBackStringStringBool callback;
    public bool isCallService = false;
#if UNITY_EDITOR
        public string strTestIAP = "";
#endif
        public bool IsInited
		{
			get
			{
				return this.IsInitialized();
            }
        }

        private static UnityPurchasingHelper instance;

        public static UnityPurchasingHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject container = new GameObject("UnityPurchasingHelper");
                    instance = container.AddComponent<UnityPurchasingHelper>();

                }
                return instance;

            }
        }

        void Awake()
        {
            DontDestroyOnLoad(this);
            IsInitialized();
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            this.controller = controller;
            this.extensions = extensions;


            /*IGooglePlayStoreExtensions m_GooglePlayExtensions = extensions.GetExtension<IGooglePlayStoreExtensions>();
            // Overall Purchasing system, configured with products for this application.
            IStoreController m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            IExtensionProvider m_StoreExtensionProvider = extensions;
            Dictionary<string, string> Dict = m_GooglePlayExtensions.GetProductJSONDictionary();
            SubscribeStatus userSubStatus = SubscribeStatus.None;
            foreach (Product item in controller.products.all)
            {
                
                if (item.definition.type.Equals(ProductType.Subscription) == true)
                {

                    Debug.LogWarning(JsonUtility.ToJson(item));
                    if (item.receipt != null)
                    {
                        string json = (Dict == null || !Dict.ContainsKey(item.definition.storeSpecificId)) ? null : Dict[item.definition.storeSpecificId];
                        SubscriptionManager s = new SubscriptionManager(item, json);
                        SubscriptionInfo info = s.getSubscriptionInfo();
                        if (info.isExpired() == Result.False)
                        {
                            userSubStatus = SubscribeStatus.Activing;
                            break;
                        }
                        else
                        {
                            userSubStatus = SubscribeStatus.Pending;
                            break;
                        }
                    }
                    else
                    {
                        userSubStatus = SubscribeStatus.Canceled;
                    }
                }
            }
            StatisticsData.Get().updateSubscriptionStatus(userSubStatus);*/
        }
        public void CallServiceGetProductInAppId()
        {
            if (isCallService == false)
            {
                LoadingManager.Instance.ENABLE = true;
                API.Instance.RequestGetProductInAppId(GetProductInAppIdSuccess);
            }else
            {
                //đã call rồi ko call nữa
            }
        }

        private void GetProductInAppIdSuccess(string _json)
        {
            LoadingManager.Instance.ENABLE = false;
            if (this.IsInitialized())
            {
                return;
            }
            JSONNode node = JSONNode.Parse(_json);
            Debug.Log(" GetProductInAppIdSuccess==== " + _json);
            //if (node["status"].AsInt == 1)
            //{
                //JSONArray data = node["data"].AsArray;
                ConfigurationBuilder configurationBuilder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance(), new IPurchasingModule[0]);
                List<string> lstProductID = new List<string>();
                foreach(JSONArray arrayItem in node.Childs)
                {
                    foreach (JSONNode item in arrayItem)
                    {
                        lstProductID.Add(item.Value);
                    }
                    
                }

                foreach (string value in lstProductID)
                {//quan trong dong nay add tat ca product_id vao
                    configurationBuilder.AddProduct(value, ProductType.Consumable);
                }
                UnityPurchasing.Initialize(this, configurationBuilder);
                isCallService = true;
            //}

            
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.LogError("OnInitializeFailed=====" + error);
        }

        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
            Debug.LogError("OnPurchaseFailed=====" + i);
            //LoadingHelper.Get("UnityPurchasing").StopLoading();
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {//function mua response
            //LoadingHelper.Get("UnityPurchasing").StopLoading();
            //JSONNode node = JSONNode.Parse(e.purchasedProduct.receipt);
            //string purchaseToken = node["purchaseToken"].Value;
            //Debug.LogError("ProcessPurchase=====");

            bool validPurchase = true; // Presume valid for platforms with no R.V.

            // Unity IAP's validation logic is only included on these platforms.
#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
            // Prepare the validator with the secrets we prepared in the Editor
            // obfuscation window.
            var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
                AppleTangle.Data(), Application.identifier);

            try
            {
                //JSONNode result = JSONNode.Parse("{\"Store\":\"GooglePlay\",\"TransactionID\":\"GPA.3347-4422-2285-47980\",\"Payload\":\"{\\\"json\\\":\\\"{\\\\\\\"orderId\\\\\\\":\\\\\\\"GPA.3347-4422-2285-47980\\\\\\\",\\\\\\\"packageName\\\\\\\":\\\\\\\"royal.gamebaionline.TLMN\\\\\\\",\\\\\\\"productId\\\\\\\":\\\\\\\"gem.1\\\\\\\",\\\\\\\"purchaseTime\\\\\\\":1561713779983,\\\\\\\"purchaseState\\\\\\\":0,\\\\\\\"developerPayload\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"developerPayload\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"is_free_trial\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"has_introductory_price_trial\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"is_updated\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"accountId\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"}\\\\\\\",\\\\\\\"purchaseToken\\\\\\\":\\\\\\\"pldgdfnjgaghfejafkoppkeg.AO-J1OzDl_B4DWRJ2QLn3bR2mZGYYCXawEAliNH9yrIy8bwAmiYDb7AtBk4-t7AYW7XUEH5Y7uObzBJHwOt7k-Ebgf1CIrO-WVkJz1LxW42upCTi-UdHZKs\\\\\\\"}\\\",\\\"signature\\\":\\\"A\\\\/I73gg7HXOLb6lEXAa\\\\/t3fLBOs5IhTbvmnLjXcWkk15oWuf0LKO8eap7om0ywxXd9SmaidZU6wZSsL7SVm0iMzvuAy83y0u6rfY\\\\/5l\\\\/P9Zz6cJXdadrUrmnpK7rO9M3M5aqIELEBUmGIxJQ\\\\/ifoZvHS0p\\\\/qshINUzKxT+4FFl681\\\\/CFbF+pQb1StLHBtLb\\\\/95g5zhFjkR1SK8dptXnli4dPWCZQUMQw33l9IFcS2UeWXcjeXIaaXQfO\\\\/DTqZD7aAh+30BUSJ8O9RW37Y6RWrM6izvZelR8AEf0OOZ8L1q2E6SQ\\\\/uqj2RaHyNyZUWcu27xDkGRKcD5JBCHp63fpDOg==\\\",\\\"skuDetails\\\":\\\"{\\\\\\\"skuDetailsToken\\\\\\\":\\\\\\\"AEuhp4KSAtrwh1MwcerI0TRHwJnnTBjIjORBWfmMRA4n5hwJNN58lntXGyzldMibHxnT\\\\\\\",\\\\\\\"productId\\\\\\\":\\\\\\\"gem.1\\\\\\\",\\\\\\\"type\\\\\\\":\\\\\\\"inapp\\\\\\\",\\\\\\\"price\\\\\\\":\\\\\\\"23.000\u00a0₫\\\\\\\",\\\\\\\"price_amount_micros\\\\\\\":23000000000,\\\\\\\"price_currency_code\\\\\\\":\\\\\\\"VND\\\\\\\",\\\\\\\"title\\\\\\\":\\\\\\\"20 Gem (Royal - Tien Len Mien Nam Online)\\\\\\\",\\\\\\\"description\\\\\\\":\\\\\\\"20 Gem\\\\\\\"}\\\",\\\"isPurchaseHistorySupported\\\":true}\"}");
                JSONNode result = JSONNode.Parse(e.purchasedProduct.receipt);
                JSONNode Payload = result["Payload"];
                //JSONNode json = Payload["json"];
                JSONNode result2 = JSONNode.Parse(Payload);
                JSONNode jsonObj = result2["json"];
                JSONNode result3 = JSONNode.Parse(jsonObj);
                string productId = result3["productId"].Value;
                string purchaseToken = result3["purchaseToken"].Value;

                callback(productId, purchaseToken, validPurchase);



                /*
                IPurchaseReceipt[] result = validator.Validate(e.purchasedProduct.receipt);

                Debug.Log("Receipt is valid. Contents:");
                foreach (IPurchaseReceipt productReceipt in result) {
                    Debug.Log(productReceipt.productID);
                    Debug.Log(productReceipt.purchaseDate);
                    Debug.Log(productReceipt.transactionID);

                    GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
                    if (null != google) {
                        // This is Google's Order ID.
                        // Note that it is null when testing in the sandbox
                        // because Google's sandbox does not provide Order IDs.
                        Debug.Log(google.transactionID);
                        Debug.Log(google.purchaseState);
                        Debug.Log(google.purchaseToken);
                        callback(productReceipt.productID, google.purchaseToken, validPurchase);
                    }

                    AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
                    if (null != apple) {
                        Debug.Log(apple.originalTransactionIdentifier);
                        Debug.Log(apple.subscriptionExpirationDate);
                        Debug.Log(apple.cancellationDate);
                        Debug.Log(apple.quantity);
                        
                    }
                    
                }*/
            }
            catch (IAPSecurityException)
            {
                Debug.Log("Invalid receipt, not unlocking content");
                validPurchase = false;
                callback("", "", validPurchase);
            }
            
#endif

            //if (validPurchase)
            // {
            //xu ly cong tien,..... o day mua thanh cong roi nek
            // }
            return PurchaseProcessingResult.Complete;
        }


        public ProductMetadata GetProductMetadata(string id)
        {
            try
            {
                Product product = controller.products.all.ToList<Product>().Find((Product e) => e.definition.id.Equals(id));
                if (product != null)
                {
                    return product.metadata;
                }
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log(ex.Message + "     " + id );
            }
            return null;
        }
        
        //PurchasingPackage a co the thay bang product_id
        public void OnPurchaseClicked(string productID, onCallBackStringStringBool _cb)
		{
            callback = null;
            callback = _cb;


            if (this.controller == null)
            {
                callback("","", false);
                return;
            }
            if (this.controller.products == null)
            {
            callback("", "", false);
            return;
            }
            if (this.controller.products.all == null)
            {
            callback("", "", false);
            return;
            }
            Product product = this.controller.products.all.ToList<Product>().Find((Product e) => productID.Equals(e.definition.id));
            if (product != null)
            {
                //LoadingHelper.Get("UnityPurchasing").ShowLoading(null, null, string.Empty); //e dang show loading o day

                this.controller.InitiatePurchase(product);
            }
        }

		/*public void StartInitialize()
		{
            if (this.IsInitialized())
            {
                return;
            }
            ConfigurationBuilder configurationBuilder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance(), new IPurchasingModule[0]);
            List<string> lstProductID = new List<string>
            {
                "aaa",
            };
            foreach (string value in lstProductID)
            {//quan trong dong nay add tat ca product_id vao
                configurationBuilder.AddProduct(value, ProductType.Consumable);
            }
            UnityPurchasing.Initialize(this, configurationBuilder);
        }*/

        private void onAddNewProductSuccess()
        {

        }
        private bool IsInitialized()
        {
            return this.controller != null && this.extensions != null;
        }

        //private PurchasingPackage ValidatorPayload(string content)
        //{
        //    Dictionary<string, object> dictionary = MiniJSON.Json.Deserialize(content) as Dictionary<string, object>;
        //    string text = (string)dictionary["Store"];
        //    try
        //    {
        //        if (text != null)
        //        {
        //            if (text == "GooglePlay")
        //            {
        //                Dictionary<string, object> dictionary2 = MiniJSON.Json.Deserialize(dictionary["Payload"].ToString()) as Dictionary<string, object>;
        //                Dictionary<string, object> dictionary3 = MiniJSON.Json.Deserialize(dictionary2["json"].ToString()) as Dictionary<string, object>;
        //                dictionary3 = (MiniJSON.Json.Deserialize(dictionary3["developerPayload"].ToString()) as Dictionary<string, object>);
        //                string @string = Encoding.Default.GetString(Convert.FromBase64String(dictionary3["developerPayload"].ToString()));
        //                return JsonUtility.FromJson<PurchasingPackage>(@string);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        UnityEngine.Debug.Log(ex.Message);
        //    }
        //    return null;
        //}
        //public void checkAndSendPurchase()
        //{
        //    List<PurchasingPackage> lstPurchaseUnsend;
        //    if (StatisticsData.Get().getPurchaseCount() > 0)
        //    {
        //        lstPurchaseUnsend = new List<PurchasingPackage>();
        //        for (int i = 0; i < StatisticsData.Get().Purchaseds.Count; i++)
        //        {
        //            if (StatisticsData.Get().Purchaseds[i].isSended == false)
        //            {
        //                lstPurchaseUnsend.Add(StatisticsData.Get().Purchaseds[i]);
        //            }
        //        }
        //        sendStepByStepService(lstPurchaseUnsend);
        //    }
            
        //}


       

        private IStoreController controller;

        private IExtensionProvider extensions;

        //private List<PurchasingEevet> delegateList = new List<PurchasingEevet>();
	}
}
