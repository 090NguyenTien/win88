﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockItemBoom : MonoBehaviour
{
    Animator MyAnim;
    // Start is called before the first frame update
    void Awake()
    {
        MyAnim = gameObject.GetComponent<Animator>();
       
    }

    public void StartAnim()
    {
        gameObject.SetActive(true);
        MyAnim.SetBool("START", true);
        MyAnim.SetBool("STOP", false);
    }

    public void StoptAnim()
    {       
        MyAnim.SetBool("START", false);
        MyAnim.SetBool("STOP", true);
        gameObject.SetActive(false);
    }
}
