﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class UpdateItem : MonoBehaviour
{
    [SerializeField]
    Text TxtNameItem, TxtLevelUp, TxtDoanhThu, TxtDiamonPrice, TxtChipPrice;
    [SerializeField]
    Image ImgItem;
    string MyId = "";
    string MyId_Rieng = "";
    [SerializeField]
    MadControll Mad;
    [SerializeField]
    EstateControll estate;
    private int ChangeType = -1;

    public long GiaChip, GiaGem;
    [SerializeField]
    GameObject Alert;
    [SerializeField]
    Text TxtAlert;
    [SerializeField]
    GameObject BtnGEM, PanelScroll;
    long TienUp = 5000000;
    long GiaNangCap = 0;
    string Type = "";
    long MyDoanhThu = 0;

    public void Init(string ID, string Id_Rieng, string _name, string level, long DoanhThu, string type, bool ByGem = false)
    {
        MyId = ID;
        MyId_Rieng = Id_Rieng;
        Type = type;
        MyDoanhThu = DoanhThu;
        // ImgItem.sprite = DataHelper.dictSprite_House[ID];
        string Img = DataHelper.DuLieuTenImgTheoID[ID];
        API.Instance.Load_SprItem(Img, ImgItem);
        TxtNameItem.text = _name;
        TxtLevelUp.text = level;

        TxtDoanhThu.text = Utilities.GetStringMoneyByLong(DoanhThu);
        //TxtChipPrice.text = Utilities.GetStringMoneyByLong(ChipPrice);
      //  BtnGEM.SetActive(false);
        this.gameObject.SetActive(true);




        My_Item it = new My_Item();
        if (type == "house" )
        {
            it = DataHelper.DuLieuNha[MyId];
        }
        else if (type == "shop")
        {
            it = DataHelper.DuLieuShop[MyId];
        }
        else if (type == "car")
        {
            it = DataHelper.DuLieuXe[MyId];
        }





        long level_long = long.Parse(level);
        if (ByGem == false)
        {
           
            long giamua = it.chip;
            GiaNangCap =(giamua * level_long)/5000000;
            
        }
        else
        {
            long giamua = it.gem;
            GiaNangCap = (giamua * TienUp * level_long) / 5000000;
        }
        //TxtChipPrice.text = GiaNangCap.ToString();
       // TxtChipPrice.text = Utilities.GetStringMoneyByLong(GiaNangCap);
        TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(GiaNangCap);
        //if (DiamonPrice > 0)
        //{
        //    TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(DiamonPrice);
        //    BtnGEM.SetActive(true);
        //}
        //else
        //{
        //    BtnGEM.SetActive(false);

       // BtnGEM

        //GiaChip = ChipPrice;
        //GiaGem = DiamonPrice;
    }






    long GiaChipUpDate()
    {
        long gia = 0;
        return gia;
    }

    public void BtnBackOnClick()
    {
        this.gameObject.SetActive(false);
       // ChangeType = -1;
    }

    public void ClouseAlert()
    {
        Alert.SetActive(false);
    }

    public void ThongBaoLoi(string msg)
    {
        TxtAlert.text = msg;
        Alert.SetActive(true);
    }

    public void MuaBangGem()
    {
        if (MyInfo.GEM >= GiaNangCap)
        {
            //TxtAlert.text = "Nâng cấp thành công";
            //Alert.SetActive(true);

            API.Instance.RequesNangCapVatPham(MyId_Rieng, RspNangCapVatPham);
        }
        else
        {
            TxtAlert.text = "Bạn không đủ GEM nâng cấp vật phẩm này";
            Alert.SetActive(true);
        }


    }


    void RspNangCapVatPham(string _json)
    {
        Debug.LogWarning("DU LIEU NÂNG CẤP SAN PHAM: " + _json);
        JSONNode node = JSONNode.Parse(_json);
        string sta = node["status"].Value;
        long chip = long.Parse(node["chip"].Value);
        long gem = long.Parse(node["gem"].Value);
        string id = node["asset_id"].Value;
        string level = node["level"].Value;
        Debug.LogWarning("fffffffffffffff --------------- " + id + "-------MyId_Rieng---------------- " + MyId_Rieng);
        long level_long = long.Parse(level);
        if (sta == "1")
        {
            upgradeDuLieuVatPhamMoiNangCap(id, level_long);
            MyInfo.CHIP = chip;
            MyInfo.GEM = gem;
            estate.CapNhatLEFT();
            My_Item_SoHuu it = DataHelper.DuLieuSoHuu_V2[id];

            bool inmad = it.InMad;
            if (inmad == true)
            {
                int vitri = 0;
                foreach (var item in DataHelper.DuLieuMap)
                {
                    string idVatPham = item.Value.id;
                    if (idVatPham == id)
                    {
                        vitri = item.Key;
                        item.Value.Level = level_long;
                        item.Value.DoanhThu = MyDoanhThu;
                    }
                }


                PanelScroll.SetActive(false);
                Mad.EditInfoItemInMad(vitri, level_long, MyDoanhThu, true);
            }
            else
            {
                if (Type == "house")
                {
                    PanelScroll.SetActive(true);
                    PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemHomeCroll();
                }
                else if (Type == "shop")
                {
                    PanelScroll.SetActive(true);
                    PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemShopInScoll();
                }
                else if (Type == "car")
                {
                    PanelScroll.SetActive(true);
                    PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemCarInScoll();
                }
            }

           
            this.gameObject.SetActive(false);
        }
        else
        {
            string MSG = node["msg"].Value;
            // PaneBuy.ThongBaoLoi(MSG);
            TxtAlert.text = MSG;
            Alert.SetActive(true);
            Debug.LogWarning("LỔI NÂNG CẤP SAN PHAM: " + MSG);
        }

       // Debug.LogWarning("st= " + sta + "  chip= " + chip + "   gem= " + gem + "   id= " + idTONG);
    }


    void upgradeDuLieuVatPhamMoiNangCap(string Id, long level)
    {
        foreach (var item in DataHelper.DuLieuSoHuu_V2)
        {
            if (Id == item.Key)
            {
                //My_Item_SoHuu it = item.Value;
                //it.Level = level;
                item.Value.Level = level;
               
            }
            
        }
    }


}
