﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuKienController : MonoBehaviour
{    
    List<ButtonMenuComp> ButtonMenuList = new List<ButtonMenuComp>();
    [SerializeField]
    ButtonMenuComp ButtonMenuComp;
    [SerializeField]
    Text txtThele, txtTimeSukien;
    [SerializeField]
    GameObject GiftChip, GiftGem;
    [SerializeField]
    GameObject Container, ContainerResult;
    [SerializeField]
    Button BtnResult;
    JSONNode curSukien;
    [SerializeField]
    ItemTopSuKien itemTopSukien;
    [SerializeField]
    GameObject PopupResult;
    // Start is called before the first frame update
    void Start()
    {
        PopupResult.SetActive(false);
        //Get Data From Service
        WWWForm form = new WWWForm();
        form.AddField("", "");
        API.Instance.BaseCallService("/eventnews/get", form, GetInfoSuKienComplete);
    }

    private void GetInfoSuKienComplete(string s)
    {
        Debug.Log("eventnews/get====" + s);
        for(int z=0;z< Container.transform.childCount; z++)
        {
            Destroy(Container.transform.GetChild(0).gameObject);
        }
        JSONNode node = JSONNode.Parse(s);
        JSONNode data = node["data"];
        if (data.Count == 0)
        {
            AlertController.api.showAlert("Hiện Tại Chưa Có Sự Kiện Nào!");
            CloseSuKien();
            return;
        }
        else
        {
            for (int i = 0; i < data.Count; i++)
            {
                ButtonMenuComp comp = Instantiate(ButtonMenuComp, Container.transform) as ButtonMenuComp;
                ButtonMenuList.Add(comp);
                comp.Init(data[i], OnSuKienClick);
                comp.GetComponent<Toggle>().group = Container.GetComponent<ToggleGroup>();
                if (i == 0)
                {
                    comp.ChangeStatus();
                    OnSuKienClick(data[i]);
                }
            }
        }
        

       

    }
    private void OnSuKienClick(JSONNode data)
    {
        //Debug.Log(data);
        curSukien = data;
        if (data["result"].Value == "null")
        {
            BtnResult.enabled = false;
            BtnResult.targetGraphic.CrossFadeAlpha(0.5f, 0, false);
        }
        else
        {
            BtnResult.enabled = true;
            BtnResult.targetGraphic.CrossFadeAlpha(1, 0, false);
        }
        txtThele.text = data["description"].Value;
        if (int.Parse(data["bonus"]["chip"].Value) == 0)
        {
            GiftChip.SetActive(false);
        }
        else
        {
            GiftChip.SetActive(true);
            GiftChip.GetComponentInChildren<Text>().text = Utilities.GetStringMoneyByLong(long.Parse(data["bonus"]["chip"].Value));
        }
        if (int.Parse(data["bonus"]["gem"].Value) == 0)
        {
            GiftGem.SetActive(false);
        }
        else
        {
            GiftGem.SetActive(true);
            GiftGem.GetComponentInChildren<Text>().text = data["bonus"]["gem"].Value;
        }        

        txtTimeSukien.text = "Sự kiện bắt đầu Từ : " + data["start_time"].Value +
            " Đến : " + data["end_time"].Value;
        ClosePopupResult();
    }
    public void ResultOnClick()
    {
        if (curSukien["result"].Value == "null") return;
        for (int z = 0; z < ContainerResult.transform.childCount; z++)
        {
            ItemTopSuKien itemTopTrsf = (ContainerResult.transform.GetChild(z)).GetComponent<ItemTopSuKien>() ;
            Destroy(itemTopTrsf.gameObject);
        }
        JSONNode result = curSukien["result"];
        for(int i = 0; i < result.Count; i++)
        {
            ItemTopSuKien itemTop = Instantiate(itemTopSukien, ContainerResult.transform) as ItemTopSuKien;
            itemTop.Init((i + 1).ToString(), result[i]);
        }
        PopupResult.SetActive(true);
    }
    public void ClosePopupResult()
    {
        PopupResult.SetActive(false);
    }
    public void CloseSuKien()
    {
        gameObject.SetActive(false);
        Destroy(gameObject);
    }
}
