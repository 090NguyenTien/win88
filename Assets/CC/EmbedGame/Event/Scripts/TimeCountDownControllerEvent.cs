﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using DG.Tweening;
using SimpleJSON;

public class TimeCountDownControllerEvent : MonoBehaviour
{
    [SerializeField]
    private Text txtTime;

    [SerializeField]
    private bool isCompleteDisable = true;

    [SerializeField]
    private string textComplete = "";

    private int timeCountDown; // tinh don vi la giay
    //private int time
    private bool isInited;
    private int timeInit;//thoi diem init tinh bang giay
    private Action callBack;
    private int baseCountDownTime = 0;
    [SerializeField]
    bool isGetTimeFromService;//xem là lấy thời gian từ service lúc đầu hay set thời gian vào từ nơi khác
    [NonSerialized]
    public bool isStartEvent;

    public void SetTxtTime(string val)
    {
        txtTime.text = val;
    }

    public bool isRunning()
    {
        return isInited;
    }
    /// <summary>
    /// count down with time in second
    /// </summary>
    /// <param name="baseTimeCountDown">base time (ex: 3600)</param>
    /// <param name="timeSecondCountDown">time need countdown (ex: 3000)</param>
    /// <param name="timeInit">time begin init</param>
    /// <param name="callBack">call back done</param>
    public void initTimeCountDown(int baseTimeCountDown,int timeSecondCountDown, int timeInit, Action callBack)
    {
        this.callBack = null;
        this.callBack = callBack;
        this.timeInit = timeInit + (int)Time.realtimeSinceStartup;
        this.timeCountDown =  timeSecondCountDown;
        this.baseCountDownTime = baseTimeCountDown;
        gameObject.SetActive(true);
        isInited = true;
        //Debug.Log("baseTimeCountDown "+ baseTimeCountDown);
        if (gameObject.activeInHierarchy == true)
        {
            StartCoroutine(UpdateTime());
        }

    }
    void Start()
    {
        if (isGetTimeFromService)
        {
            //API.Instance.RequestTimeEventRemain(RspTimeEventRemain);
            GamePacket gp = new GamePacket(EventCommandKey.GET_EVENT_TIME);
            SFS.Instance.SendZoneRequest(gp);
        }
        else
        {
            //initTimeCountDown(0, 1000, 200, CompleteCountDown);
        }
        
    }
    public void OnSFSResponse(GamePacket gp)
    {
        switch (gp.cmd)
        {
            case EventCommandKey.GET_EVENT_TIME:
                LoadingManager.Instance.ENABLE = false;
                RspTimeEventRemain(gp);
                break;
        }
    }
    private void RspTimeEventRemain(GamePacket gp)
    {
        var isStart = gp.GetBool("isstart");
        var difftime = gp.param.GetLong("diftime");
        if (isStart)//nếu isstart = true thì diftime là thời gian đếm ngươc đến kết thúc
        {
            txtTime.text = "Đang Bắt Đầu";
        }
        else//nếu isstart = false thì thời gian đếm ngược đến sự kiện tiếp theo
        {
            initTimeCountDown(0, int.Parse(difftime.ToString()), 0, CompleteCountDown);
        }
        //initTimeCountDown(0, difftime, 0, CompleteCountDown);
    }

    private void CompleteCountDown()
    {
        if (callBack != null)
        {
            callBack();
        }
    }

    void OnEnable()
    {
        if (isInited)
        {
            StartCoroutine(UpdateTime());
        }
    }

    IEnumerator UpdateTime()
    {
        while (true)
        {
            int curTime = (int)Time.realtimeSinceStartup;
            //Debug.LogError("==UpdateTime==============curTime"+ curTime);
            int currentCountdown = timeCountDown - (curTime - timeInit);
            //Debug.LogError("==UpdateTime==============currentCountdown" + currentCountdown);
            if (currentCountdown > 0)
            {
                //if (isStartEvent)
                //{
                //    txtTime.text = "Bắt Đầu";
                //}
                //else
                //{
                    txtTime.text = Ulti.convertCountDownTimeToTextShowSecond(currentCountdown);
                //}              
                
            }
            else
            {
                isInited = false;
                if (isCompleteDisable == false)
                {
                    txtTime.text = textComplete;
                }
                //Debug.Log("==UpdateTime==============isCompleteDisable" + isCompleteDisable);
                gameObject.SetActive(!isCompleteDisable);
                if (callBack != null)
                {
                    callBack();
                }
                yield break;
            }
            yield return new WaitForSeconds(1);
        }
    }
    public int getTimeLeft()
    {
        return timeCountDown - ((int)Time.realtimeSinceStartup - timeInit);

    }
}
