﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChipToGemController : MonoBehaviour
{
    int fee, tile;
    [SerializeField]
    InputField chipChange, gemReceive;
    [SerializeField]
    Text txtDescription, txtUser, chipUser, gemUser;

    // Start is called before the first frame update
    void Start()
    {
        if (!GameHelper.IS_LOAD_LOGIN)
        {
            GameHelper.ChangeScene(GameScene.LoginScene);
            return;
        }
        txtUser.text = MyInfo.NAME;
        chipUser.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        gemUser.text = MyInfo.GEM.ToString();
        gemReceive.characterLimit = 10;       
        //Adds a listener that invokes the "LockInput" method when the player finishes editing the main input field.
        //Passes the main input field into the method when "LockInput" is invoked
        gemReceive.onEndEdit.AddListener(delegate { LockInput(gemReceive); });
        //call service get info init
        API.Instance.RequestGetChipToGemInfo(RspGetInitInfo);
    }
    // Checks if there is anything entered into the input field.
    void LockInput(InputField input)
    {
        if (input.text.Length > 0)
        {
            long gemEx = long.Parse(input.text);
            long chipEx = gemEx * tile;
            if (chipEx > MyInfo.CHIP)
            {                
                gemEx = MyInfo.CHIP / tile;
                chipEx = gemEx * tile;
            }
            chipChange.text = Utilities.GetStringMoneyByLong(chipEx+(chipEx*fee/100));
            gemReceive.text = gemEx.ToString();
            //if(chipEx < tile)
            //{
            //    gemReceive.text = "0";
            //}
            //else
            //{
            //    gemReceive.text = Math.Round(chipEx * fee / 100f) + "";
            //}            
        }
        else if (input.text.Length == 0)
        {
            chipChange.text = "0";
        }
    }
    void RspGetInitInfo(string _json)
    {
        JSONNode data = JSONNode.Parse(_json);

        //JSONNode data = node["data"];
        fee = int.Parse(data["chip_to_gem_fee"].Value);
        tile = int.Parse(data["chip_to_gem_rate"].Value);

        txtDescription.text = "Đổi Chip Qua Gem Sẽ Mất Phí là <color=yellow>" + fee + "%</color> Tỉ Lệ Đổi Là <color=yellow>1Gem = " + Utilities.GetStringMoneyByLong(tile) + " chip</color>";
    }

    public void ChangeChipToGem()
    {
        if (gemReceive.text == "")
        {
            AlertController.api.showAlert("Bạn chưa nhập số gem muốn đổi!");
            return;
        }
        int gem = int.Parse(gemReceive.text);
        if (gem == 0)
        {
            AlertController.api.showAlert("Số Gem để đổi phải lớn hơn 0!");
            return;
        }
        API.Instance.RequestChangeChipToGem(int.Parse(gemReceive.text), RspChangeChipToGem);
    }
    void RspChangeChipToGem(string _json)
    {
        JSONNode data = JSONNode.Parse(_json);

        //JSONNode data = node["data"];
        if (int.Parse(data["status"].Value) == 1)
        {
            AlertController.api.showAlert("Đổi Gem thành công!");
            MyInfo.CHIP = long.Parse(data["chip"].Value);
            MyInfo.GEM = long.Parse(data["gem"].Value);
            chipUser.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
            gemUser.text = MyInfo.GEM.ToString();

        }
        else
        {
            AlertController.api.showAlert(data["msg"]);
        }
       
    }
    public void BackToHome()
    {
        LoadingManager.Instance.ENABLE = true;
        GameHelper.ChangeScene(GameScene.HomeSceneV2);
    }
}
