﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoBangHoiController : MonoBehaviour
{
    [SerializeField]
    Text TxtTenBang, TxtBangChu, TxtBangPho, TxtThanhVien, TxtChip, TxtGem;
    string idOwner, owner_name, nameBang, deputy, deputy_name, symbol_url;
    public static int is_public, min_vip, tax;
    int max_member, total_member;
    public static long chip, gem, min_help, max_help;
    [SerializeField]
    Toggle ToggleLeaveBang, ToggleChoDuyet, ToggleCauHinh, ToggleCuuTro;
    [SerializeField]
    Image AvatarBang, AvatarTrophy;
    [SerializeField]
    GameObject BANGPHO;
    [SerializeField]
    Button BtnXinCuuTro;
    string trophy_url = "";
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void Show()
    {
        BtnXinCuuTro.gameObject.SetActive(false);
        GamePacket gp = new GamePacket(CommandKey.GET_CLAN_INFO);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void OnChatResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            case CommandKey.GET_CLAN_INFO:
                GetClanInfoRsp(param);
                break;
            case CommandKey.REQUEST_CUU_TRO:
                XinCuuTroRsp(param);
                break;
            case CommandKey.CONTRIBUTE_CLAN:
                ContributeClanRsp(param);
                break;
        }
    }
    private void ContributeClanRsp(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            string memberId = param.GetString("memberid");
            long clan_chip = param.GetLong("clan_chip");
            chip = clan_chip;
            TxtChip.text = Utilities.GetStringMoneyByLongBigSmall(chip);
            if (memberId == MyInfo.ID)
            {
                long chip = param.GetLong("chip");
                long user_chip = param.GetLong("user_chip");
                MyInfo.CHIP = user_chip;
                AlertController.api.showAlert("Quyên Góp Thành Công "+ Utilities.GetStringMoneyByLongBigSmall(chip) + " Vào Bang");
            }            
        }
        else
        {
            AlertController.api.showAlert(param.GetString("msg"));
        }
    }
    private void XinCuuTroRsp(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            AlertController.api.showAlert("Đã Gửi Mong Muốn Xin Cứu Trợ Tới Bang Chủ");
        }
        else
        {
            AlertController.api.showAlert(param.GetString("msg"));
        }
    }

    private void GetClanInfoRsp(GamePacket param)
    {
        Debug.Log(param);
        ISFSObject info = param.GetSFSObject("info");
        MyInfo.CLAN_ID = info.GetUtfString("id");
        idOwner = info.GetUtfString("owner");
        chip = info.GetLong("chip");
        owner_name = info.GetUtfString("owner_name");
        max_member = info.GetInt("max_member");
        nameBang = info.GetUtfString("name");
        is_public = info.GetInt("is_public");
        min_vip = info.GetInt("min_vip");
        tax = info.GetInt("tax");
        total_member = info.GetInt("total_member");
        gem = info.GetLong("gem");
        min_help = info.GetLong("min_help");
        max_help = info.GetLong("max_help");
        symbol_url = info.GetUtfString("symbol_url");
        trophy_url = info.GetUtfString("trophy_url");
        TxtBangPho.text = "";
        BANGPHO.SetActive(false);
        if (info.ContainsKey("deputy"))
        {
            deputy = info.GetUtfString("deputy");
            deputy_name = info.GetUtfString("deputy_name");
            TxtBangPho.text = deputy_name;
            BANGPHO.SetActive(true);
        }

        TxtTenBang.text = nameBang;
        TxtBangChu.text = owner_name;
        TxtThanhVien.text = total_member + "/" + max_member;
        TxtChip.text = Utilities.GetStringMoneyByLongBigSmall(chip);
        TxtGem.text = gem.ToString();

        if(idOwner == MyInfo.ID)
        {
            ToggleLeaveBang.gameObject.SetActive(false);
            ToggleChoDuyet.gameObject.SetActive(true);
            ToggleCauHinh.gameObject.SetActive(true);
            ToggleCuuTro.gameObject.SetActive(true);
        }
        else if(deputy == MyInfo.ID)
        {
            ToggleLeaveBang.gameObject.SetActive(true);
            ToggleChoDuyet.gameObject.SetActive(true);
            ToggleCauHinh.gameObject.SetActive(false);
            ToggleCuuTro.gameObject.SetActive(false);
            BtnXinCuuTro.gameObject.SetActive(true);
        }
        else
        {
            ToggleLeaveBang.gameObject.SetActive(true);
            ToggleChoDuyet.gameObject.SetActive(false);
            ToggleCauHinh.gameObject.SetActive(false);
            ToggleCuuTro.gameObject.SetActive(false);
            BtnXinCuuTro.gameObject.SetActive(true);
        }
        StartCoroutine(UpdateAvatarThread(symbol_url, AvatarBang));
        if(trophy_url != "")
        {
            StartCoroutine(UpdateAvatarThread(trophy_url, AvatarTrophy));
            AvatarTrophy.gameObject.SetActive(true);
        }
        else
        {
            AvatarTrophy.gameObject.SetActive(false);
        }        
    }
   
    public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        if (mainImage != null && avatar != null)
            avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
    }

}
