﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class LuckyDice : MonoBehaviour {
    public Image dice1;
    public Image dice2;
    public Image dice3;
    public GameObject mask;
    public Sprite[] dices;
    private int diceResult1;
    private int diceResult2;
    private int diceResult3;
    void Update()
    {
        //if (Input.GetKeyUp(KeyCode.Space))
        //{
        //    //ShowDice(2,4,6, true);
        //}
    }

    public void ShowDice(int dice1, int dice2, int dice3, bool isMask) {
        gameObject.SetActive(true);
        diceResult1 = dice1;
        diceResult2 = dice2;
        diceResult3 = dice3;
        if(!isMask)
            Animation();
        else
        {
			this.dice1.gameObject.transform.localScale = new Vector3 (1, 1, 1);
			this.dice2.gameObject.transform.localScale = new Vector3 (1, 1, 1);
			this.dice3.gameObject.transform.localScale = new Vector3 (1, 1, 1);
			mask.transform.localPosition = new Vector3(0, -20, 0);
            AnimationMask();
        }

    }

    private void AnimationMask()
    {
        mask.SetActive(true);
        dice1.sprite = dices[diceResult1 - 1];
        dice2.sprite = dices[diceResult2 - 1];
        dice3.sprite = dices[diceResult3 - 1];
        iTween.ShakePosition(mask, new Vector3(1, 1, 0), 4);
        StartCoroutine(FinishShakeMask(4));
    }

    private IEnumerator FinishShakeMask(int v)
    {
        yield return new WaitForSeconds(v);
        oncompleteDice();
    }

    public void Animation()
    {
        
        iTween.MoveTo(dice1.gameObject, iTween.Hash("position", new Vector3(-91, -35, 0), "islocal", true, "onupdatetarget", gameObject, "onupdate", "onUpdateDice", "time", 2));
        iTween.MoveTo(dice2.gameObject, iTween.Hash("position", new Vector3(48, -59, 0), "islocal", true, "time", 2));

        iTween.ScaleTo(dice1.gameObject, new Vector3(2, 2, 2), 2);
        iTween.ScaleTo(dice2.gameObject, new Vector3(2, 2, 2), 2);
        iTween.ScaleTo(dice3.gameObject, new Vector3(2, 2, 2), 2);

        iTween.ShakeRotation(dice1.gameObject, new Vector3(0, 0, 200), 4);
        iTween.ShakeRotation(dice2.gameObject, new Vector3(0, 0, 200), 4);
        iTween.ShakeRotation(dice3.gameObject, new Vector3(0, 0, 200), 4);

        StartCoroutine(FinishDiceFly(2));
    }

    IEnumerator FinishDiceFly(int v)
    {
        yield return new WaitForSeconds(v);
        iTween.MoveTo(dice1.gameObject, iTween.Hash("position", new Vector3(-47, -13, 0), "islocal", true, "onupdatetarget", gameObject, "onupdate", "onUpdateDice", "oncompletetarget", gameObject,
            "oncomplete", "oncompleteDice", "time", 2));
        iTween.MoveTo(dice2.gameObject, iTween.Hash("position", new Vector3(38, -28, 0), "islocal", true, "time", 2));
        iTween.ScaleTo(dice1.gameObject, new Vector3(1, 1, 1), 2);
        iTween.ScaleTo(dice2.gameObject, new Vector3(1, 1, 1), 2);
        iTween.ScaleTo(dice3.gameObject, new Vector3(1, 1, 1), 2);
    }

    public void onUpdateDice() {
        int index = UnityEngine.Random.Range(0, dices.Length - 1);
        dice1.sprite = dices[index];
        index = UnityEngine.Random.Range(0, dices.Length - 1);
        dice2.sprite = dices[index];
        index = UnityEngine.Random.Range(0, dices.Length - 1);
        dice3.sprite = dices[index];
    }

    public void oncompleteDice()
    {
        dice1.sprite = dices[diceResult1 - 1];        
        dice2.sprite = dices[diceResult2 - 1];
        dice3.sprite = dices[diceResult3 - 1];
    }
}
