﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatLuckyController : MonoBehaviour {

    // Use this for initialization
    private float scrollSpeed = 2f;
    public Text demo;
    public InputField inputchat;
    public GameObject chatContainer;
    public ScrollRect scrollRect;
    private RectTransform contenRectTransform;
    private float maxScroll;
    public GameObject container;
    void Start() {
        this.contenRectTransform = this.scrollRect.content;
        this.maxScroll = this.contenRectTransform.rect.yMax;
        container.SetActive(false);
    }

     void Update()
    {
        bool hasScrolled = this.contenRectTransform.position.y < this.maxScroll;
        if (!hasScrolled)
        {
            Move();
        }
    }
    private void Move()
    {
        Vector2 contentPosition = this.contenRectTransform.position;
        Vector2 newPosition = new Vector2(contentPosition.x, contentPosition.y + this.scrollSpeed);
        this.contenRectTransform.position = newPosition;
    }
    public void OpenChat()
    {
        container.SetActive(true);
    }
    public void CloseChat()
    {
        container.SetActive(false);
    }
    public void OnPublicMsg(string _msg)
    {
        Text msgtext = Instantiate(demo, chatContainer.transform);
        msgtext.gameObject.SetActive(true);
        msgtext.text = _msg;
    }

    public void SendChat() {
		if (inputchat.text.Length > 0) {
			SFS.Instance.SendPublicMsg ("<color=yellow>"+ MyInfo.NAME + "</color>: " + inputchat.text);
			inputchat.text = "";
		}
    }

    public void OnSFSResponse(GamePacket gp)
    {

    }
}
