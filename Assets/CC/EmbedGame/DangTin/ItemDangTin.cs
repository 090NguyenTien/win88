﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDangTin : MonoBehaviour
{
    [SerializeField]
    Text txtNguoiDang, txtNoiDung, txtVip, txtDate;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void Init(string createDate, string noidung, int vip, string nguoidang)
    {
        txtNguoiDang.text = nguoidang;
        txtNoiDung.text = noidung;
        txtDate.text = "Đăng Ngày:"+createDate;
        txtVip.text = "Vip" + vip;
    }
    
}
