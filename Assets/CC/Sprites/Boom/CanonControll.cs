﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonControll : MonoBehaviour
{
    Animator MyAnim;

    void Awake()
    {
        MyAnim = gameObject.GetComponent<Animator>();
    }


   public void BatDaiBang()
    {
        MyAnim.SetBool("DaiBang", true);
    }

    public void Huy()
    {
        Destroy(this.gameObject);
    }
}
