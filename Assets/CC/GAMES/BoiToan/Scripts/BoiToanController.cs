﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoiToanController : MonoBehaviour {
    [SerializeField]
    Image imgHuQue;
    [SerializeField]
    Button btnLac,btnClose;
    [SerializeField]
    ShopBoiToan panelShop;
    [SerializeField]
    Text txtTurn;
    [SerializeField]
    GameObject panelResult;
    [SerializeField]
    Text txtXamName, txtXamDetail, txtXamTho, txtXamSo;
    [SerializeField]
    Button btnCloseResult;
    [SerializeField]
    GameObject giftChip, giftCard, panelFadeEffect;
    [SerializeField]
    Text txtDataChip, txtDataLoaiThe;
    bool isLac = false;
	// Use this for initialization
	void Start () {
        LoadingManager.Instance.ENABLE = false;
        btnLac.onClick.AddListener(LacHuOnClick);
        btnClose.onClick.AddListener(CloseOnClick);
        btnCloseResult.onClick.AddListener(CloseResultOnClick);
        imgHuQue.GetComponent<Animator>().Play("NoAction");
        panelShop.Init();
        txtTurn.text = "x" + MyInfo.XAM_BOI_TOAN.ToString();
        panelFadeEffect.GetComponent<Animator>().Play("NoAction");
        panelFadeEffect.SetActive(false);
        panelResult.SetActive(false);

    }

    private void CloseResultOnClick()
    {
        panelResult.SetActive(false);
    }

    private void CloseOnClick()
    {
        if (isLac)
        {
            AlertController.api.showAlert("Vui Lòng Thoát Sau Khi Gieo Quẻ Xong!");
            return;
        }
        LoadingManager.Instance.ENABLE = true;
        GameHelper.ChangeScene(GameScene.HomeSceneV2);
    }

    private void LacHuOnClick()
    {
        if (MyInfo.XAM_BOI_TOAN == 0)
        {
            panelShop.Show();
            return;
        }
        if (isLac)
        {
            AlertController.api.showAlert("Đang Gieo Quẻ Vui Lòng Chờ!");
            return;
        }
        isLac = true;
        API.Instance.RequestLacQue(OnLacQueComplete);
        
    }

    private void OnLacQueComplete(string _json)
    {
        StartCoroutine(ShowResultLacQue(_json));
        
    }
    public void ShowPanelResult()
    {
        panelResult.SetActive(true);
        panelFadeEffect.SetActive(true);
        panelFadeEffect.GetComponent<Animator>().Play("PanelFadeEffect");
        imgHuQue.GetComponent<Animator>().Play("NoAction");
        isLac = false;
    }
    private IEnumerator ShowResultLacQue(string _json)
    {        
        giftCard.SetActive(false);        
        imgHuQue.GetComponent<Animator>().Play("LagHuAnimation");
        //yield return new WaitForSeconds(3);
        
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" OnLacQueComplete " + _json);
        if (node["status"].AsInt == 1)
        {
            long chipReceive = long.Parse(node["chip"]);
            //int card = int.Parse(node["card"]);
            int idXam = int.Parse(node["idxam"]);
            string nameXam = node["name"];
            string content = node["content"];
            string poem = node["poem"];
            MyInfo.XAM_BOI_TOAN = int.Parse(node["xam"]);
            txtTurn.text = "x" + node["xam"];
            MyInfo.CHIP = long.Parse(node["final_chip"]);
            //set gui data
            txtXamTho.text = poem;
            txtXamDetail.text = content;
            txtXamSo.text = idXam.ToString();
            txtXamName.text = nameXam;
            txtDataChip.text = chipReceive.ToString();

            if (node["card"].ToString().Length > 0)
            {
                giftCard.SetActive(true);
                string CardPrice = node["card"]["amount"].Value;
                string CardType = node["card"]["provider"].Value;
                txtDataLoaiThe.text = CardPrice + " " + CardType;
            }               
        }
        else
        {
            AlertController.api.showAlert("Bạn Chưa Có Xăm Để Xem. Vui Lòng Mua Thêm Xăm!");
        }
        yield return null;
    }
}
