﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum XTType{
		HIGH_CARD,
		PAIR,
		TWO_PAIRS,
		THREE_OF_A_KIND,
		STRAIGHT,
		FLUSH,
		FULL_HOUSE,
		FOUR_OF_A_KIND,
		STRAIGHT_FLUSH
}
public class XTUtils {
	


	public static XTType getCardsType(List<BaseCardInfo> cards){
		List<BaseCardInfo> usingCards = new List<BaseCardInfo> (cards);
		SortCardsList (usingCards);
		int countSameCard = CountSameCard (usingCards);
		switch (countSameCard) {
		case 2:
			return XTType.PAIR;
		case 4:
			return XTType.TWO_PAIRS;
		case 6:
			return XTType.THREE_OF_A_KIND;
		case 8:
			return XTType.FULL_HOUSE;
		case 12:
			return XTType.FOUR_OF_A_KIND;
		}
		if (isStraightFlush (usingCards))
			return XTType.STRAIGHT_FLUSH;
		if (isFlush (usingCards))
			return XTType.FLUSH;
		if (isStraight (usingCards))
			return XTType.STRAIGHT;
		return XTType.HIGH_CARD;
	}

	public static bool[] GetIndexSuccess(XTType _type, List<BaseCardInfo> _cards)
	{
		switch (_type) {
		case XTType.HIGH_CARD:
			return GetIndexHighCard (_cards);
		case XTType.FOUR_OF_A_KIND:
			return GetIndexFourOfKind (_cards);
		case XTType.PAIR:
			return GetIndexPair (_cards);
		case XTType.THREE_OF_A_KIND:
			return GetIndexThreeOfKind (_cards);
		case XTType.TWO_PAIRS:
			return GetIndexTwoPairs (_cards);
		}
		return new bool[]{ true, true, true, true, true };
	}
	static bool[] GetIndexHighCard(List<BaseCardInfo> _cards)
	{
		bool[] result = new bool[]{ false, false, false, false, false };

		//2 vi tri co Value bai giong nhau
//		int[] indexs = new int[2];

		int maxValue = _cards[0].Value;
		int indexMAX = 0;

		for (int i = 1; i < _cards.Count; i++) {
			if (_cards [i].Value > maxValue) {
				indexMAX = i;
				maxValue = _cards [i].Value;
			}
		}
		result [indexMAX] = true;
		return result;
	}
	static bool[] GetIndexFourOfKind(List<BaseCardInfo> _cards)
	{
		bool[] result = new bool[]{ true, true, true, true, true };

		//2 vi tri co Value bai khac nhau
		int[] indexs = new int[2];

		for (int i = 1; i < _cards.Count; i++) {
			if (_cards [i].Value != _cards [i - 1].Value) {
				indexs [0] = i - 1;
				indexs [1] = i;
				break;
			}
		}
		int indexFail = -1;

		for (int j = 0; j < _cards.Count; j++) {
			if (j != indexs [0] && j != indexs [1]) {
				if (_cards [j].Value != _cards [indexs [0]].Value)
					indexFail = indexs [0];
				else if (_cards [j].Value != _cards [indexs [1]].Value)
					indexFail = indexs [1];
			}
		}

		result [indexFail] = false;

		return result;
	}
	static bool[] GetIndexPair(List<BaseCardInfo> _cards)
	{
		bool[] result = new bool[]{ false, false, false, false, false };

		//2 vi tri co Value bai giong nhau
		int[] indexs = new int[2];

		for (int i = 0; i < _cards.Count; i++) {
			for (int j = i + 1;j<_cards.Count;j++){
				if (i != j){
					if (_cards[i].Value == _cards[j].Value){
						result[i] = true;
						result[j] = true;

//						Debug.Log ("INDEX PAIR: " + i + " vs " + j);
						return result;
					}
				}
			}
		}

		return result;
	}
	static bool[] GetIndexThreeOfKind(List<BaseCardInfo> _cards)
	{
		bool[] result = new bool[]{ false, false, false, false, false };

		int count = 0;

		for (int i = 0; i < _cards.Count; i++) {
			for (int j = i + 1;j<_cards.Count;j++){
				if (i != j){
					if (_cards[i].Value == _cards[j].Value){
						result[i] = true;
						result[j] = true;

//						Debug.Log ("INDEX THREE: " + i + " vs " + j);
							count ++;

						if (count == 2)
							return result;
					}
				}
			}
		}
		return result;

//		List<BaseCardInfo> usingCards = new List<BaseCardInfo> (_cards);
//		SortCardsList (usingCards);
//
//		if (_cards [2].Value == _cards [1].Value) {
//			result [3] = false;
//			result [4] = false;
//		} else {
//			result [0] = false;
//			result [1] = false;
//		}

		return result;
	}
	static bool[] GetIndexTwoPairs(List<BaseCardInfo> _cards)
	{
//		bool[] result = new bool[]{ true, true, true, true, true };
//
//		List<BaseCardInfo> usingCards = new List<BaseCardInfo> (_cards);
//		SortCardsList (usingCards);
//
//		if (_cards [0].Value != _cards [1].Value) {
//			result [0] = false;
//		} else if (_cards [2].Value != _cards [3].Value) {
//			result [2] = false;
//		} else
//			result [4] = false;

		bool[] result = new bool[]{ false, false, false, false, false };

		int count = 0;

		for (int i = 0; i < _cards.Count; i++) {
			for (int j = i + 1;j<_cards.Count;j++){
				if (i != j){
					if (_cards[i].Value == _cards[j].Value){
						result[i] = true;
						result[j] = true;

//						Debug.Log ("INDEX TWO PAIRS: " + i + " vs " + j);
						count ++;

						if (count == 2)
							return result;
					}
				}
			}
		}
		return result;

		return result;
	}


	private static int CountSameCard(List<BaseCardInfo> cards){
		int count = 0;
		foreach (BaseCardInfo card in cards) {
			foreach (BaseCardInfo _card in cards) {
				if (card.Type != _card.Type && card.Value == _card.Value) {
//                    Debug.Log(card.Id + " vs " + _card.Id);
                
					count++;
//                    Debug.Log("=== " + count);
				}
			}
		}
//		Debug.Log ("Same: " + count);
		return count;
	}

	private static bool isStraightFlush(List<BaseCardInfo> cards){
		if (cards.Count < 5)
			return false;
		BaseCardInfo previousCard = cards [0];
		for (int i = 1; i < 5; i++) {
			if (previousCard.Type != cards [i].Type || previousCard.Value + 1 != cards [i].Value)
				return false;
			previousCard = cards [i];
		}
		return true;
	}

		private static bool isFlush(List<BaseCardInfo> cards){
			if(cards.Count != 5)
				return false;
			foreach (BaseCardInfo card in cards) {
				if(card.Type != cards[0].Type)
					return false;
			}
			return true;
		}

		private static bool isStraight(List<BaseCardInfo> cards){
			if(cards.Count != 5)
				return false;
			BaseCardInfo previous = cards[0];
			for(int i = 1; i < 5; i++){
				if((previous.Value + 1 != cards[i].Value))
					return false;
				previous = cards[i];
			}
			return true;
		}

		private static void SortCardsList(List<BaseCardInfo> cards){
		cards.Sort(delegate (BaseCardInfo x, BaseCardInfo y)
			{
				if (x.Value == y.Value)
					return ((int)x.Type).CompareTo((int)y.Type);
				return x.Value.CompareTo(y.Value);
			});
	}
//			Collections.sort(cards, new Comparator<BaseCardInfo>(){
//				@Override
//				public int compare(Card c1, Card c2) {
//					if(c1.Value > c2.Value)
//						return 1;
//					else if(c1.Value < c2.Value)
//						return -1;
//					else {
//						if(c1.Type < c2.Type)
//							return -1;
//						if(c1.Type > c2.Type)
//							return 1;
//					}
//					return 0;
//				}
//			});
//		}

		private static int compareHighCards(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
		BaseCardInfo card1 = list1[list1.Count - 1];
		BaseCardInfo card2 = list2[list2.Count - 1];
			if(card1.Value < card2.Value)
				return -1;
			else if(card1.Value > card2.Value)
				return 1;
			if(card1.Type < card2.Type)
				return -1;
			else if(card1.Type > card2.Type)
				return 1;
			return 0;
		}

		private static int comparePairs(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
			int pair1Value = 0, pair2Value = 0;

			for(int i = 0; i < list1.Count; i++){
				int count = 0;
				for(int j = 0; j < list1.Count; j++){
					if(i != j && list1[i].Value == list1[j].Value)
						count++;
				}
				if(count == 1){
					pair1Value = list1[i].Value;
					break;
				}
			}
			for(int i = 0; i < list2.Count; i++){
				int count = 0;
				for(int j = 0; j < list2.Count; j++){
					if(i != j && list2[i].Value == list2[j].Value)
						count++;
				}
				if(count == 1){
					pair2Value = list2[i].Value;
					break;
				}
			}

			if(pair1Value < pair2Value)
				return -1;
			else if(pair1Value > pair2Value)
				return 1;

			foreach (BaseCardInfo card in list1) {
			if(card.Value == pair1Value && (int)card.Type == 3)
					return 1;
			}

			foreach (BaseCardInfo card in list2) {
			if(card.Value == pair2Value && (int)card.Type == 3)
					return -1;
			}
			return 0;
		}

		private static int compareTwoPairs(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
			int pair11Value = list1[3].Value, pair12Value = list1[1].Value;
			int pair21Value = list2[3].Value, pair22Value = list2[1].Value;

			if(pair11Value < pair21Value)
				return -1;
			if(pair11Value > pair21Value)
				return 1;
			if(pair12Value < pair22Value)
				return -1;
			if(pair12Value > pair22Value)
				return 1;

			int maxPairValue = pair11Value > pair12Value ? pair11Value : pair12Value;
			foreach (BaseCardInfo card in list1) {
			if(card.Value == maxPairValue && (int)card.Type == 3)
					return 1;
			}
			foreach (BaseCardInfo card in list2) {
			if(card.Value == maxPairValue && (int)card.Type == 3)
					return -1;
			}
			return 0;
		}

		private static int compareThreeOfAKind(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
			int value1 = list1[2].Value, value2 = list2[2].Value;
			if(value1 < value2)
				return -1;
			else if(value1 > value2)
				return 1;
			return 0;
		}

		private static int compareStraight(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
		BaseCardInfo card1 = list1 [list1.Count - 1];
		BaseCardInfo card2 = list2 [list2.Count - 1];
			if(card1.Value < card2.Value)
				return -1;
			else if(card1.Value > card2.Value)
				return 1;
			if(card1.Type < card2.Type)
				return -1;
			else if(card1.Type > card2.Type)
				return 1;
			return 0;
	}


		private static int compareFlush(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
			return compareStraight(list1, list2);
		}

	public static int compare(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
		List<BaseCardInfo> usingList1 = new List<BaseCardInfo>(list1);
		List<BaseCardInfo> usingList2 = new List<BaseCardInfo>(list2);
			SortCardsList(usingList1);
			SortCardsList(usingList2);
			XTType type1 = getCardsType(usingList1);
			XTType type2 = getCardsType(usingList2);
		if((int)type1 < (int)type2)
				return -1;
		else if((int)type1 > (int)type2)
				return 1;
			switch (type1) {
		case XTType.HIGH_CARD:
				return compareHighCards(usingList1, usingList2);
		case XTType.PAIR:
				return comparePairs(usingList1, usingList2);
		case XTType.TWO_PAIRS:
				return compareTwoPairs(usingList1, usingList2);
		case XTType.THREE_OF_A_KIND:
				return compareThreeOfAKind(usingList1, usingList2);
		case XTType.STRAIGHT:
				return compareStraight(usingList1, usingList2);
		case XTType.FLUSH:
				return compareFlush(usingList1, usingList2);
			default:
				return compareHighCards(usingList1, usingList2);
			}
		}

		/*public static void main(String[] args) {
		List<BaseCardInfo> deck = ZoneExtension.getInstance().getCards(GameName.XI_TO);
		List<BaseCardInfo> xiToDeck = new List<>();
		foreach (BaseCardInfo card in deck) {
			if(card.Value >= 7)
				xiToDeck.add(card);
		}
		
		List<BaseCardInfo> card1 = new List<>();
	}*/
}