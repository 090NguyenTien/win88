﻿public class DelimiterKey
{
    public const char comma = ',';
	public const char semicolon = ';';
	public const char hash = '#';
	public const char underscore = '_';
	public const char colon = ':';
	public const char dollarPack = '$';
    public const char verticalBarPack = '|';
}
