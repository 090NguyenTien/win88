﻿using DG.Tweening;
using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DuanguaController : MonoBehaviour {
    public List<ItemBetHorseData> lstItemBet;
    private SFS sfs;
    public GameObject panelGameRun, horseController, panelReady;
    //private bool isTest = true;
    public List<Button> BetButtons;
    public static long ChoosingBetMoney=10000;
    
    [SerializeField]
    UserInfoDuaNgua UserInfoDuaNgua;
    [SerializeField]
    GameObject panelRemainTime,TxtRemainNextRun, TxtTimeBet;
    [SerializeField]
    GameObject panelBetBoard, bgBetBoardPanel, panelResultEndRace, panelHistory;
    string slotHorseWin;
    [SerializeField]
    List<Text> lstHistoryWin = new List<Text>();
    [SerializeField]
    ListBetWhenHorseRun lstBetWhenHorseRun;
    private void Awake()
    {
        sfs = SFS.Instance;        
    }
    void Start()
    {  
        panelGameRun.SetActive(false);
        panelRemainTime.SetActive(false);
        panelResultEndRace.SetActive(false);
        panelHistory.SetActive(false);
        lstBetWhenHorseRun.gameObject.SetActive(false);
        panelHistory.GetComponent<PanelResultEndRace>().ClearData();
        Init();
    }
    public void Init()
    {
        bgBetBoardPanel.gameObject.SetActive(true);
        UserInfoDuaNgua.Init();
        panelBetBoard.SetActive(true);
        DisableAllBetButtons();
        BetButtons[0].transform.GetChild(1).gameObject.SetActive(false);
        //call server get Data
        GamePacket gp = new GamePacket(CommandKey.GET_GAME_ROOM_INFO);
        sfs.SendRoomRequest(gp);
        //Test
        //int[] horses = { 1, 2, 3, 4, 5, 6 };
        //StartRun(horses);
        //end

    }
    private void initBetBoard()
    {
        bgBetBoardPanel.gameObject.SetActive(true);
        panelBetBoard.SetActive(true);
        panelGameRun.SetActive(false);
        panelRemainTime.SetActive(false);
        horseController.SetActive(false);
        horseController.GetComponent<HorseController>().EndRace();
    }
    public void OnSFSResponse(GamePacket gp)
    {
        switch (gp.cmd)
        {           
            case CommandKey.GET_GAME_ROOM_INFO:
                GetGameRoomInfoRes(gp);
                break;
            case CommandKey.RESULT_RACING_HORSE:
                BeginRacingHorse(gp);
                break;
            case CommandKey.RESULT_RACING_HORSE_COMPLETE:
                EndRacingHorse(gp);
                break;
            case CommandKey.BET:
                BetHorseRes(gp);
                break;
            case CommandKey.HISTORY_USER_BET:
                ShowHistoryUserBet(gp);
                break;
            case CommandKey.USER_EXIT:           
                GameHelper.ChangeScene(GameScene.HomeSceneV2);
                break;
        }
    }
    public void GetHistoryUserBet()
    {
        sfs.GetUserBetHorse(new GamePacket(CommandKey.HISTORY_USER_BET));
    }
    private void ShowHistoryUserBet(GamePacket gp)
    {
        Debug.LogWarning("HISTORY_USER_BET==============" + gp.param.ToJson());
        if (gp.ContainKey("history"))
        {
            panelHistory.SetActive(true);
            ISFSArray history = gp.GetSFSArray("history");
            ISFSObject obj;
            for (int i = 0; i < history.Size(); i++)
            {
                obj = (SFSObject)history.GetSFSObject(i);
                long chipwin = obj.GetLong("chipwin");                
                long chipBet = obj.GetLong("chip");
                string slot = obj.GetUtfString("slot");
                int ratio = obj.GetInt("ratio");
                panelHistory.GetComponent<PanelResultEndRace>().ShowHistoryBet(slot, chipBet, chipwin,i, ratio);
            }
        }
        panelHistory.GetComponent<PanelResultEndRace>().ShowHorseWin("");
        if (gp.ContainKey("horsewin"))
        {
            int[] horsewin = gp.GetIntArray("horsewin");
            //Debug.LogError("horsewin==============" + horsewin.ToString());
            string slotwin = horsewin[0] < horsewin[1] ? horsewin[0].ToString() + "-" + horsewin[1].ToString() : horsewin[1].ToString() + "-" + horsewin[0].ToString();
            // Debug.LogError("slotHorseWin==============" + slotHorseWin);
            int horsewinratio = gp.GetInt("horsewinratio");
            panelHistory.GetComponent<PanelResultEndRace>().ShowHorseWin(slotwin + "<color=red>"+"  x"+ horsewinratio.ToString()+"</color>");
        }       
    }

    private void BetHorseRes(GamePacket gp)
    {
        Debug.LogWarning("BetHorseRes==============" + gp.param.ToJson());
        //uname,slot,bm;
        string slot = gp.GetString("slot");
        long bm = gp.GetLong("bm");
        ItemBetHorseData item = getItemBoardBetById(slot);
        item.GetComponent<Animator>().Play("BetHorse", 0, 0.25f);
        if (gp.ContainKey("uname"))
        {
            string uname = gp.GetString("uname");
            if(uname == MyInfo.NAME)
            {
                if (MyInfo.CHIP >= bm)
                {
                    MyInfo.CHIP -= bm;
                    UserInfoDuaNgua.updateChipUser(MyInfo.CHIP);
                    lstBetWhenHorseRun.SetData(slot, gp.GetInt("ratio"),bm);
                }
            }           
        }

        item.UpdateBetSlot(bm);
        UserInfoDuaNgua.updateBetMoney(bm);
        //getItemBoardBetById();
    }
    private void EndRacingHorse(GamePacket gp)
    {
        LoadingManager.Instance.setPlayMusic(true);
        SoundManager.StopSound(SoundManager.DUANGUA_RACE);
        horseController.GetComponent<HorseController>().StopParticleEndRace();
        resetDataMyBet(gp);
        panelReady.GetComponent<PanelReady>().Hide();
        horseController.GetComponent<HorseController>().targetLine.SetActive(false);
        Debug.LogWarning("RESULT_RACING_HORSE_COMPLETE==============" + gp.param.ToJson());
        PanelResultEndRace panelResultEndRaceClass = panelResultEndRace.GetComponent<PanelResultEndRace>();
        if (gp.ContainKey("result"))
        {
            ISFSArray result = gp.GetSFSArray("result");
            ISFSObject obj;
            long chipUser = gp.GetLong("chip");
            UserInfoDuaNgua.updateChipUser(chipUser);
            long userChipWin = 0;            
            for (int i = 0; i < result.Size(); i++)
            {                
                obj = (SFSObject)result.GetSFSObject(i);
                string slot = obj.GetUtfString("slot");
                long chipwin = obj.GetLong("chipwin");
                if (chipwin > 0)
                {
                    StartCoroutine(AnimatePlayerGetMoney(chipwin,panelResultEndRaceClass.getHorseMyPetBySlot(slot).transform.position));
                    userChipWin = chipwin;
                }
                long chipBet = obj.GetLong("chip");
                
                panelResultEndRaceClass.SetHorseBetBySlot(slot,chipBet,chipwin);
            }            
            long curChipUser = gp.GetLong("chip");
            MyInfo.CHIP = curChipUser;
            panelResultEndRaceClass.EnableTextKhongCuoc(false);
        }
        else
        {
            panelResultEndRaceClass.EnableTextKhongCuoc(true);
        }
        
        long timebet = gp.GetLong("timebet");
        
        int second = (int)timebet / 1000;
        StartCoroutine(showTimeBetRemain(second));
        BetFakeClient();//
        initBetBoard();
        addDataBoardBet(gp);
        ShowResultRace(gp.GetInt("ratio"));
        setHistoryWin(gp);
        addNewDataBoardForPanel(gp);
        lstBetWhenHorseRun.Init();//khoi tao moi danh sach ngua da~ Cuoc o Giao Dien GamePlay
        lstBetWhenHorseRun.gameObject.SetActive(false);
        //MyInfo.CheckMoney(UserInfoDuaNgua.getTxtChipUser().gameObject);
        
    }


    private IEnumerator AnimatePlayerGetMoney(long money,Vector3 beginPos)
    {   
        PanelResultEndRace panelResultEndRaceClass = panelResultEndRace.GetComponent<PanelResultEndRace>();
        Image imgGold = panelResultEndRaceClass.ImgGoldAnimation;
        imgGold.gameObject.SetActive(true);
        imgGold.gameObject.transform.position = beginPos;
        imgGold.transform.DOMove(UserInfoDuaNgua.getTxtChipUser().transform.position, 0.6f, false);
        yield return new WaitForSeconds(0.6f);
        UserInfoDuaNgua.getTxtChipUser().GetComponent<Animator>().Play("BetHorse", 0, 0.25f);
        imgGold.gameObject.SetActive(false);        

        yield return null;
    }
    private void ShowResultRace(int ratio)
    {   
        panelResultEndRace.GetComponent<PanelResultEndRace>().ShowHorseWin(slotHorseWin + "  x"+ ratio.ToString());
    }
    private void addNewDataBoardForPanel(GamePacket gp)
    {
        panelResultEndRace.GetComponent<PanelResultEndRace>().AddNewDataBoard(gp);
    }
    private void BeginRacingHorse(GamePacket gp)
    {
        StopBetFakeClient();
        Debug.LogWarning("RESULT_RACING_HORSE==============" + gp.param.ToJson());
        currCountdownValue = 0;
        LoadingManager.Instance.setPlayMusic(false);
        SoundManager.PlaySound(SoundManager.DUANGUA_RACE);        
        bgBetBoardPanel.gameObject.SetActive(false);
        panelRemainTime.SetActive(false);
        panelBetBoard.SetActive(false);
        int[] horsewin = gp.GetIntArray("horsewin");        
        StartRun(horsewin);
        //Debug.LogError("horsewin==============" + horsewin.ToString());
        slotHorseWin = horsewin[0] < horsewin[1] ? horsewin[0].ToString() + "-" + horsewin[1].ToString() : horsewin[1].ToString() + "-" + horsewin[0].ToString();
        // Debug.LogError("slotHorseWin==============" + slotHorseWin);
        panelHistory.GetComponent<PanelResultEndRace>().Hide();
        panelResultEndRace.GetComponent<PanelResultEndRace>().Hide();
        lstBetWhenHorseRun.gameObject.SetActive(true);
    }
    
    private void GetGameRoomInfoRes(GamePacket gp)
    {
        LoadingManager.Instance.ENABLE = false;
        SoundManager.PlaySound(SoundManager.ENTER_ROOM);        
        Debug.LogWarning("GetGameRoomInfoRes==============" + gp.param.ToJson());        
        addDataBoardBet(gp);
        addDataResultEndRace(gp);
        UserInfoDuaNgua.ShowInfoUser(gp.GetString("uname"), gp.GetLong("chip"), MyInfo.sprAvatar); //Add Data Config User Info
        addDataHistoryMyBet(gp);
        if (gp.GetBool("isrun"))//Ngua chay Roi thi se co thêm biến kết quả horsewin
        {
            long timeremain = gp.GetLong("timeremain");
            int second = (int)timeremain / 1000;
            StartCoroutine(showTimeNextTurn(second));
        }
        else
        {
            long timebet = gp.GetLong("timebet");
            int second = (int)timebet / 1000;
            StartCoroutine(showTimeBetRemain(second));
            BetFakeClient();
        }
        if (gp.ContainKey("horsewin"))
        {
            int[] horsewin = gp.GetIntArray("horsewin");
            //Debug.LogError("horsewin==============" + horsewin.ToString());
            slotHorseWin = horsewin[0] < horsewin[1] ? horsewin[0].ToString() + "-" + horsewin[1].ToString() : horsewin[1].ToString() + "-" + horsewin[0].ToString();
            //Debug.LogError("slotHorseWin==============" + slotHorseWin);
        }
        setHistoryWin(gp);
        lstBetWhenHorseRun.Init();//khoi tao moi danh sach ngua da~ Cuoc o Giao Dien GamePlay
    }

    private void BetFakeClient()
    {
        foreach (ItemBetHorseData item in lstItemBet)
        {
            item.StopRunFake = false;
            StartCoroutine(item.RunFakeBetAnimation());
        }
    }
    private void StopBetFakeClient()
    {
        foreach (ItemBetHorseData item in lstItemBet)
        {
            item.StopRunFake = true;
        }
    }
    private void setHistoryWin(GamePacket gp)
    {
        if (gp.ContainKey("slot_win_history"))
        {
            string[] slotWinHistory = gp.GetStringArray("slot_win_history");
            int[] slotWinRatioHistory = gp.GetIntArray("slot_win_ratio_history");
            int index = 0;
            foreach (string slotHis in slotWinHistory)
            {
                lstHistoryWin[index].text = slotHis + "<color=red>" +"  x" + slotWinRatioHistory[index].ToString()+"</color>";
                index++;
            }
        }
    }

    private void addDataResultEndRace(GamePacket gp)
    {
        //Add Data Config Board To Result Panel
        panelResultEndRace.GetComponent<PanelResultEndRace>().Init(gp);
        //end
    }

    private int currCountdownValue;
    private IEnumerator showTimeBetRemain(int countdownValue = 10)
    {        
        currCountdownValue = countdownValue;
        while (currCountdownValue > 0)
        {
            int cnt = currCountdownValue - 3;//chay nhanh hon 3 giay để hiển thị thông báo nhà cái ngừng cược
            if (cnt < 0) cnt = 0;
            TxtTimeBet.GetComponent<Text>().text = Ulti.convertCountDownTimeToText(cnt);            
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
            if(currCountdownValue <= 3)
            {
                 panelReady.GetComponent<PanelReady>().StopReceiveBet();
                 StopBetFakeClient();
            }           
        }
    }
    
    private int currCountNextTurn;
    private IEnumerator showTimeNextTurn(int countdownValue = 10)
    {
        panelRemainTime.SetActive(true);
        currCountNextTurn = countdownValue;
        while (currCountNextTurn > 0)
        {
            TxtRemainNextRun.GetComponent<Text>().text = Ulti.convertCountDownTimeToText(currCountNextTurn);            
            yield return new WaitForSeconds(1.0f);
            currCountNextTurn--;
        }
    }
    private void addDataHistoryMyBet(GamePacket gp)
    {
        if (gp.ContainKey("myBets"))
        {
            ISFSObject myBets = gp.GetSFSObject("myBets");
            string[] slots;
            slots = myBets.GetKeys();
            long sumMyBet = 0;
            foreach (string slot in slots)
            {
                sumMyBet += myBets.GetLong(slot);
            }      
            UserInfoDuaNgua.updateInfoBet(MyInfo.CHIP, sumMyBet.ToString(), "0");
        }
    }
    private void resetDataMyBet(GamePacket gp)
    {  
        long chipuser = MyInfo.CHIP;
        if (gp.ContainKey("chip")) chipuser = gp.GetLong("chip");
        UserInfoDuaNgua.updateInfoBet(chipuser,"0", "0");
    }

    private void addDataBoardBet(GamePacket gp)
    {
        //Add Data Config To Board
        ISFSArray board_bet = gp.GetSFSArray("board_bet");        
        SFSObject obj;
        string id;
        int ratio;
        ItemBetHorseData itemBet;
        ISFSObject uBets = gp.GetSFSObject("uBets");
        long uBet;
        for (int i = 0; i < board_bet.Size(); i++)
        {
            obj = (SFSObject)board_bet.GetSFSObject(i);           
            id = obj.GetUtfString("slot");//1-2,1-3...5-6
            ratio = obj.GetInt("ratio");
            itemBet = getItemBoardBetById(id);
            itemBet.SetRatioBetSlot(ratio);
            //itemBet.TxtRatio.text = "x" + ratio.ToString();
            //Add Data All User Bet
            if (gp.ContainKey("uBets") && uBets.ContainsKey(id))
            {
                uBet = uBets.GetLong(id);
                itemBet.SetSumBetSlot(uBet);
            }
            //end
        }
        //end
    }
    private ItemBetHorseData getItemBoardBetById(string id)
    {
        foreach(ItemBetHorseData item in lstItemBet)
        {
            if (item.id == id) return item;
        }
        return null;
    }
    
    public void OnBetMoneyButtonClick(Button button)
    {
        //SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        //DisableAllBetButtons();
        //EnableButton(button);
        //string betMoneyText = button.transform.GetChild(0).GetComponent<Text>().text;
        //ChoosingBetMoney = TXUtil.ConvertShortTextToMoney(betMoneyText);
    }
    public void DisableAllBetButtons()
    {
        foreach (Button button in BetButtons)
        {
            button.transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    public void EnableButton(Button button)
    {
        DisableAllBetButtons();
        button.transform.GetChild(1).gameObject.SetActive(false);
        string betMoneyText = button.transform.GetChild(0).GetComponent<Text>().text;
        ChoosingBetMoney = TXUtil.ConvertShortTextToMoney(betMoneyText);
    }
    private void SendGetGameRoomInfoRequest()
    {
        GamePacket gp = new GamePacket(CommandKey.GET_GAME_ROOM_INFO);
        sfs.SendRoomRequest(gp);
    }
    private void StartRun(int[] horsewin)
    {
        panelReady.GetComponent<PanelReady>().Ready();
        panelGameRun.GetComponent<ScrollingBackground>().Show();
        horseController.GetComponent<HorseController>().Run(horsewin);
    }
}
