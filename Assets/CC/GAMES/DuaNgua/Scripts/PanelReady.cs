﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelReady : MonoBehaviour {
    [SerializeField]
    Text TxtStatus;
    int currCountdownValue;

    public void Ready()
    {
        TxtStatus.text = "";
        gameObject.SetActive(true);
        StartCoroutine(showTimerReamain(3));
    }

    private IEnumerator showTimerReamain(int countdownValue = 3)
    {
        currCountdownValue = countdownValue;
        while (currCountdownValue > 0)
        {
            TxtStatus.text = "READY";//Ulti.convertCountDownTimeToText(currCountdownValue-1);
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
            if (currCountdownValue == 0)
            {
                TxtStatus.text = "START!";
                yield return new WaitForSeconds(1.0f);               
                gameObject.SetActive(false);
            }
        }
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void WaitingStatus(string horseWin)
    {
        gameObject.SetActive(true);
        TxtStatus.supportRichText = true;
        TxtStatus.text = "CẶP NGỰA VỀ NHẤT \n <color=white>" + horseWin + "</color>" + "\n" + "KẾT THÚC LƯỢT ĐUA. CHỜ THỐNG KÊ KẾT QUẢ...";       
    }
    public void StopReceiveBet()
    {
        gameObject.SetActive(true);
        TxtStatus.text = "HẾT THỜI GIAN NHẬN CƯỢC...";
    }
 }
