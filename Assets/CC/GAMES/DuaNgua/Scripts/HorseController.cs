﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorseController : MonoBehaviour {

    public List<Horse> lstHorse = new List<Horse>();
    public GameObject panelGameRun, targetLine, panelReady;
    private HorseController horseController;
    private int[] horsewin;
    [SerializeField]
    ParticleSystem particleEndRace;
    // Use this for initialization
    void Start () {
        horseController = gameObject.GetComponent<HorseController>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Init()
    {
        Horse horse;
        for(int i=0;i<lstHorse.Count;i++)
        {
            horse = lstHorse[i];
            int rank = getRankById((i + 1));
            horse.Init(rank+1, (i + 1).ToString());
            horse.Run();
        }
        countHorseComplete = 0;
        targetLine.SetActive(false);
    }
    private int getRankById(int id)
    {
        for(int i=0;i< horsewin.Length;i++)
        {
            if((int)horsewin[i] == id)
            {
                return i;
            }
        }
        return -1;
    }
    public int countHorseComplete;//số lượng ngựa đã về đích
    public void HorseWinCompleteRace()//cap ngua 2 con đã về đích
    {
        countHorseComplete++;
        Horse horse;
        for (int i = 0; i < lstHorse.Count; i++)
        {
            horse = lstHorse[i];
            horse.ValueRandom = 1;
        }
        //panelGameRun.GetComponent<ScrollingBackground>().Stop();
    }
    public IEnumerator ShowResultEndRace()
    {
        particleEndRace.Play();
        yield return new WaitForSeconds(1.0f);
        if (panelGameRun && panelGameRun.activeSelf)//truong hợp đang trong game play thì mới xử lý
        {
            string slotwin = horsewin[0] < horsewin[1] ? horsewin[0].ToString() + "-" + horsewin[1].ToString() : horsewin[1].ToString() + "-" + horsewin[0].ToString();
            panelReady.GetComponent<PanelReady>().WaitingStatus(slotwin);
            //EndRace();
        }        
    }
    public void StopParticleEndRace()
    {
        particleEndRace.Stop();
    }
    public void EndRace()
    {        
        Time.timeScale = 1f;
        Horse horse;
        for (int i = 0; i < lstHorse.Count; i++)
        {
            horse = lstHorse[i];
            horse.Stop();
        }
        panelGameRun.GetComponent<ScrollingBackground>().Stop();
    }
    public void Run(int[] horsewin)
    {
        this.horsewin = horsewin;
        gameObject.SetActive(true);
        Init();
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void showTargetLine()
    { 
        targetLine.SetActive(true);
        panelGameRun.GetComponent<ScrollingBackground>().Stop();
    }
}
