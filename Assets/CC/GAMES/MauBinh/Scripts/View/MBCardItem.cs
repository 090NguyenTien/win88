﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MBCardItem : BaseCardItem {

//	[SerializeField]
	GameObject backCard;

    [SerializeField]
    private GameObject hideCardBG = null; //lop den che card
    public bool backCardBackground
    {
        set
        {
            hideCardBG.SetActive(value);
        }
    }

	public bool BackCard
    {
		set{
			if (backCard == null)
				backCard = transform.Find ("BackCard").gameObject;
			backCard.SetActive (value);
		}
	}
//	public void OnSelect()
//	{
//		string currentSceneName = SceneManager.GetActiveScene().name;
//		if(currentSceneName.Equals(GameScene.TLMNScene.ToString()))
//			TLMNRoomController.Instance.player.gameObject.SendMessage("PickCard", this, SendMessageOptions.DontRequireReceiver);
//	}


}
