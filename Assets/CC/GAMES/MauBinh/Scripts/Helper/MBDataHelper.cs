﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MBDataHelper : MonoBehaviour {

		public static MBDataHelper instance;

		[SerializeField]
		List<Sprite> lstWin, lstValueRow, lstNumber;

		public void Init()
		{
				instance = this;
		}

		public Sprite GetSprRow(MBType _type)
		{
				return lstValueRow[(int)_type];
		}
		public Sprite GetSprWin(MBTypeImmediate _type)
		{
				return lstWin [(int)_type];
		}
		public Sprite GetSprNumber(int _number)
		{
				return lstNumber [_number];
		}
}
